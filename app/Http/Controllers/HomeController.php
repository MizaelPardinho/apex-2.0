<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if ($user) {

            $checkCookie = DB::table('users')->where([
                'cpf' => $user->cpf,
                'termo_cookie' => 1
            ])->count();
        }

        return view('home', [
            'checkCookie' => !empty($checkCookie) ? 1 : 0
        ]);
    }

    public function setarCheckCookie()
    {
        $user = User::where('cpf', Auth::user()->cpf)->first();

        if ($user) {
            $user->termo_cookie = 1;
            $user->save();
            return response()->json(true);
        }

        return response()->json(false);
    }
}
