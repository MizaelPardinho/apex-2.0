<?php

namespace App\Http\Controllers\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastro-usuario.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->only(
            'nome',
            'celular',
            'email',
            'cpfUsuario',
            'senha'
        ), $this->rules(), $this->messages());

        if ($validate->fails()) {
            return redirect()
                ->route('cadastro-usuario')
                ->withInput()
                ->withErrors($validate);
        }

        DB::table('users')->insert([
            'name' => $request->nome,
            'telefone' => $request->telefone,
            'celular' => $request->celular,
            'email' => $request->email,
            'cpf' => $request->cpfUsuario,
            'password' => bcrypt($request->senha)
        ]);

        Log::criarLog($request->cpfUsuario, 'Usuário com CPF: ' . $request->cpfUsuario . ' cadastrado com sucesso');

        return redirect()
            ->route('login')
            ->with('usuarioCadastradoComSucesso', 'Usuário cadastrado com sucesso!')
            ->with('cpf', $request->cpfUsuario);
    }

    public function forgetPassword(Request $request)
    {
        $user = DB::table('users')->where('cpf', $request->cpf)->first();

        if (!$user) {
            return redirect()->back()->withErrors(['cpf' => trans('CPF não cadastrado.')]);
        }

        $tokenData = Str::random();

        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => $tokenData,
            'created_at' => Carbon::now()
        ]);

        if ($this->sendResetEmail($user, $tokenData)) {
            Log::criarLog($user->cpf, 'Iniciou o processo de recuperação de senha pelo e-mail');

            return redirect()->back()->with(
                'status',
                "Um link de recuperação de senha foi enviado para o seguinte e-mail cadastrado: $user->email"
            );
        } else {
            return redirect()->back()->withErrors(['error' => trans('Erro de rede. Por favor Tente Novamente em alguns instantes.')]);
        }
    }

    private function sendResetEmail($user, $tokenData)
    {
        try {
            Mail::to($user->email)->send(new ResetPassword($tokenData, $user));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function resetPassword(Request $request)
    {
        GoogleRecaptcha::verify(
            '6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        $tokenReset = DB::table('password_resets')
            ->where('token', $request->token)
            ->first();

        if (!$tokenReset) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'Token inválido']);
        }

        $user = User::where('email', $tokenReset->email)->first();

        if (!$user) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'Usuário não encontrado, verifique se as informações estão corretas']);
        }

        if ($request->password != $request->passwordconfirm) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'As senhas informadas não são iguais']);
        }

        $user->password = bcrypt($request->password);
        $user->save();

        Auth::login($user);

        DB::table('password_resets')
            ->where('email', $user->email)
            ->delete();

        Log::criarLog($user->cpf, 'Senha alterada com sucesso no processo de recuperar senha');

        return redirect()->route('home');
    }

    /**
     * Validation rules
     * @return array
     */
    private function rules()
    {
        return [
            'nome' => 'required',
            'celular' => 'required',
            'email' => 'required|unique:users,email',
            'cpfUsuario' => 'required|unique:users,cpf',
            'senha' => 'required'
        ];
    }

    /**
     * Validation rules messages
     * @return array
     */
    private function messages()
    {
        return [
            'nome.required' => 'O campo nome é obrigatório',
            'celular.required' => 'O campo celular é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'cpfUsuario.required' => 'O campo CPF é obrigatório',
            'senha.required' => 'O campo senha é obrigatório',
            'email.unique' => 'O campo E-mail já está sendo utilizado',
            'cpfUsuario.unique' => 'O campo CPF já está sendo utilizado'
        ];
    }
}
