<?php

namespace App\Http\Controllers\FaleConosco;

use App\Http\Controllers\Controller;
use App\Mail\FaleConosco;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FaleConoscoController extends Controller
{
    public function index()
    {
        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'mensagem',
            'no_debug',
            'mt-2 mb-3 ml-1',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        $processosAquisicoes =  DB::connection('lecom')
            ->table('v_aquisicoes')
            ->select('numero', 'processo_apex')
            ->get();

        return view('faleconosco.index', [
            'pagina' => "Geral",
            'showRecaptcha'  => $showRecaptcha,
            'processosAquisicoes' => $processosAquisicoes
        ]);
    }

    public function enviarMensagem(Request $request)
    {
        GoogleRecaptcha::verify('6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG', 'Por favor clique no checkbox do reCAPTCHA primeiro!');

        $request->validate([
            'nome' => 'required',
            'email' => 'email|required',
            'telefone' => 'required',
            'assunto' => 'required',
            'mensagem' => 'required'
        ], [
            'nome.required' => 'O campo é obrigatório',
            'email.email' => 'O campo precisa ser um e-mail válido',
            'email.required' => 'O campo é obrigatório',
            'telefone.required' => 'O campo é obrigatório',
            'assunto.required' => 'O campo é obrigatório',
            'mensagem.required' => 'O campo é obrigatório'
        ]);

        // Modificar o E-mail para conta padrão
        Mail::to('fernanda.chamma@plano.inf.br')->send(new FaleConosco($request));

        if (!empty($request->numero_processo)) {

            $contato = DB::connection('lecom')
                ->table('v_aquisicoes')
                ->select('email_representante_apex')
                ->where('processo_apex', $request->numero_processo)
                ->first();

            if (!empty($contato->email_representante_apex)) {
                Mail::to($contato->email_representante_apex)->send(new FaleConosco($request));
            }
        }

        request()->session()->flash(
            'message',
            'Mensagem enviada com sucesso!'
        );

        return redirect()->route('faleconosco');
    }
}
