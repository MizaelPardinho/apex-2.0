<?php

namespace App\Http\Controllers\Linguagem;

use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class IdiomaController extends Controller
{

    public function defineIdioma($locale)
    {
        $languages = ['pt-br', 'en', 'es'];

        if (in_array($locale, $languages)) {
            session(['localex' => $locale]);

            $user = Auth::user();

            if ($user) {
                Log::criarLog($user->cpf, 'Alteração de idioma para "' . $locale . '"');
            }
        }

        return redirect()->route('home');
    }
}
