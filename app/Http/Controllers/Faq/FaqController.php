<?php

namespace App\Http\Controllers\Faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog($user->cpf, 'Acesso a página Faq');
        }

        return view('faq.index', [
            'faq' => DB::connection('lecom')
                ->table('v_apex_faq')
                ->get()
        ]);
    }

    public function ajuda(Request $request)
    {
        $contexto =  $request->pagina;

        if ($contexto != "Geral" && $contexto != "" && !is_null($contexto)) {
            $faq = DB::connection('lecom')
                ->table('v_apex_faq')
                ->where("contexto", "=", $contexto)
                ->get();
        } else {
            $faq = DB::connection('lecom')
                ->table('v_apex_faq')
                ->get();
        }

        return view('ajuda.index', [
            'faq' => $faq
        ]);
    }
}
