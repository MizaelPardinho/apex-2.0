<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validarUsuario(Request $request)
    {
        $user = User::where('cpf', $request['cpf_login'])->first();

        if ($user) {
            return redirect()->route('login')->with('cpf', $request['cpf_login']);
        } else {
            return redirect()->route('cadastro-usuario')->with('cpf', $request['cpf_login']);
        }
    }

    public function verificarCheckTermoLGPD(Request $request)
    {
        $cpf = $request->cpf;

        if ($cpf) {
            $termoCheck = DB::table('users')->where([
                'cpf' => $cpf,
                'termo_lgpd' => 1
            ])->count();

            if ($termoCheck) {
                return response()->json(['termoCheck' => true]);
            }
        }

        return response()->json(['termoCheck' => false]);
    }

    public function setarCheckTermoLGPD(Request $request)
    {
        $user = User::where('cpf', $request->cpf)->first();

        if ($user) {
            $user->termo_lgpd = 1;
            $user->save();

            return response()->json(true);
        }

        return response()->json(false);
    }
}
