<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'passwordconfirm',
            'no_debug',
            'mt-3 mb-3',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email, 'showRecaptcha' => $showRecaptcha]
        );
    }
}
