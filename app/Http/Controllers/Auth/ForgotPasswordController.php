<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'cpf',
            'no_debug',
            'mt-3 mb-3',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('auth.passwords.email', ['showRecaptcha' => $showRecaptcha]);
    }
}
