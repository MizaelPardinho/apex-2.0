<?php

namespace App\Http\Controllers\Fornecedores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Traits\HandleArquivos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FornecedoresController extends Controller
{
    use HandleArquivos;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::criarLog(Auth::user()->cpf, 'Acesso a página Fornecedores');

        return view('fornecedores.index', [
            'pagina' => "Forncedores",
            'fornecedor' => DB::connection('integracao')
                ->table('lecom_apex_fornecedores')
                ->where('cpf_responsavel', Auth::user()->cpf)
                ->where('status', '!=', 'exclusao')
                ->whereNull('data_exclusao')
                ->first()

        ]);
    }

    public function carregarInformacoesApartirDoCNPJ(Request $request)
    {
        $cnpj = remove_character_document($request->cnpj);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.receitaws.com.br/v1/cnpj/" . $cnpj . "/days/30",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer b21989b535bde93f103ef1254ca087f703d704615e13bff434282c6abee13d97"
            ),
        ));

        $response =  json_decode(curl_exec($curl));
        curl_close($curl);

        return response()->json($response);
    }

    public function create()
    {
        Log::criarLog(Auth::user()->cpf, 'Acesso a página para cadastrar fornecedor');

        return view('fornecedores.cadastrar-fornecedor', [
            'pagina' => "Forncedores",
            'produtos' => DB::connection('lecom')
                ->table('apex_catalogo_serv_prd')
                ->select('codigo', 'nome', 'unidade', 'valor')
                ->whereNull('dt_exclusao')
                ->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validate->fails()) {

            session()->forget('produtosJaSelecionados');

            if (!empty($request->produtos)) {

                $produtosJaSelecionados = [];

                foreach ($request->produtos as $codigoProduto) {
                    $produtosJaSelecionados[$codigoProduto] = $codigoProduto;
                }

                session()->push('produtosJaSelecionados', $produtosJaSelecionados);
            }

            return redirect()
                ->route('cadastrar-fornecedor')
                ->withInput()
                ->withErrors($validate);
        }

        $existeCnpj = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cnpj', $request->cnpj)
            ->whereNull('data_exclusao')
            ->count();

        if ($existeCnpj) {
            return redirect()
                ->route('cadastrar-fornecedor')
                ->withInput()
                ->with('message', "Atenção: O CNPJ {$request->cnpj} já se encontra cadastrado.");
        }

        if (empty($request->produtos)) {
            return redirect()
                ->route('cadastrar-fornecedor')
                ->withInput()
                ->with('message', 'Por favor, adicione ao menos um produto/serviço.');
        }

        $idFornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->insertGetId([
                'cpf_responsavel' => Auth::user()->cpf,
                'nome_fantasia' => $request->nomeFantasia,
                'razao_social' => $request->razaoSocial,
                'cnpj' => $request->cnpj,
                'inscricao_estadual' => $request->inscricaoEstadual,
                'ramo_atuacao' => $request->ramoAtuacao,
                'nome_representante' => $request->nomeRepresentante,
                'email_representante' => $request->emailRepresentante,
                'telefone' => $request->telefone,
                'cep' => $request->cep,
                'endereco' => $request->endereco,
                'bairro' => $request->bairro,
                'municipio' => $request->municipio,
                'estado' => $request->estado,
                'pais' => $request->pais,
                'status' => 'inclusao',
                'data_alteracao' => date('Y-m-d')
            ]);

        session()->forget('produtosJaSelecionados');

        $this->salvarDocumentosNaBaseDeDados($idFornecedor, Auth::user()->cpf);

        if ($request->produtos) {
            foreach ($request->produtos as $codigoProduto) {
                DB::connection('integracao')
                    ->table('lecom_apex_fornecedor_produtos_servicos')
                    ->insert([
                        'id_fornecedor' => $idFornecedor,
                        'codigo' => $codigoProduto,
                        'status' => 'inclusao',
                        'data_alteracao' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        Log::criarLog(Auth::user()->cpf, 'Fornecedor cadastrado com sucesso');

        return redirect()
            ->route('gerenciar-fornecedor')
            ->with('fornecedorCadastradoComSucesso', 'Fornecedor cadastrado com sucesso!');
    }

    private function salvarDocumentosNaBaseDeDados($idFornecedor, $cpfResponsavel)
    {
        if (session()->has('documentacao_fornecedores')) {
            foreach (session('documentacao_fornecedores') as $dados) {

                $documentoJaExiste = DB::connection('integracao')
                    ->table('lecom_apex_documentos_fornecedores')
                    ->where([
                        'id_fornecedor' => $idFornecedor,
                        'descricao' => $dados->descricao
                    ])->count();

                if ($documentoJaExiste) {
                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_fornecedores')
                        ->where('id_fornecedor', $idFornecedor)
                        ->where('documento', $dados->caminho)
                        ->update([
                            'status' => 'inclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                } else {
                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_fornecedores')
                        ->insert([
                            'id_fornecedor' => $idFornecedor,
                            'cpf_responsavel' => $cpfResponsavel,
                            'descricao' => $dados->descricao,
                            'documento' => $dados->caminho,
                            'status' => 'inclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                }
            }

            Log::criarLog(Auth::user()->cpf, 'Salvando documentos do fornecedor');

            session()->forget("documentacao_fornecedores");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $idFornecedor
     * @return \Illuminate\Http\Response
     */
    public function edit($idFornecedor)
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('id', $idFornecedor)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {

            $documentos = DB::connection('integracao')
                ->table('lecom_apex_documentos_fornecedores')
                ->where('id_fornecedor', $fornecedor->id)
                ->where('status', '!=', 'exclusao')
                ->get();

            if ($documentos) {
                session()->forget('documentacao_fornecedores');

                foreach ($documentos as $documento) {
                    session()->push('documentacao_fornecedores', (object) [
                        'descricao' => $documento->descricao,
                        'arquivo' => $documento->documento,
                        'caminho' => $documento->documento
                    ]);
                }
            }

            $produtos = DB::connection('integracao')
                ->table('lecom_apex_fornecedor_produtos_servicos')
                ->where('id_fornecedor', $fornecedor->id)
                ->where('status', '!=', 'exclusao')
                ->get();

            if ($produtos) {
                $produtosFiltrados = [];

                foreach ($produtos as $produto) {
                    $produtosFiltrados[$produto->codigo] = $produto;
                }
            }

            Log::criarLog(Auth::user()->cpf, 'Acesso a página para alterar o fornecedor');

            return view('fornecedores.editar-fornecedor', [
                'pagina' => "Forncedores",
                'produtos' => DB::connection('lecom')
                    ->table('apex_catalogo_serv_prd')
                    ->select('codigo', 'nome')
                    ->whereNull('dt_exclusao')
                    ->get(),
                'fornecedor' => $fornecedor,
                'documentos' => $documentos,
                'produtosJaSelecionados' => $produtosFiltrados
            ]);
        }

        return redirect()
            ->route('gerenciar-fornecedor');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $idFornecedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idFornecedor)
    {
        $validate = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validate->fails()) {
            return redirect()
                ->route('editar-fornecedor', $idFornecedor)
                ->withInput()
                ->withErrors($validate);
        }

        if (empty($request->produtos)) {
            return redirect()
                ->route('editar-fornecedor', $idFornecedor)
                ->withInput()
                ->with('error', 'Por favor, adicione ao menos um produto/serviço.');
        }

        $cpfResponsavel = Auth::user()->cpf;

        DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where([
                'cpf_responsavel' => $cpfResponsavel,
                'id' => $idFornecedor
            ])->update([
                'cpf_responsavel' => $cpfResponsavel,
                'nome_fantasia' => $request->nomeFantasia,
                'razao_social' => $request->razaoSocial,
                'cnpj' => $request->cnpj,
                'inscricao_estadual' => $request->inscricaoEstadual,
                'ramo_atuacao' => $request->ramoAtuacao,
                'nome_representante' => $request->nomeRepresentante,
                'email_representante' => $request->emailRepresentante,
                'telefone' => $request->telefone,
                'cep' => $request->cep,
                'endereco' => $request->endereco,
                'bairro' => $request->bairro,
                'municipio' => $request->municipio,
                'estado' => $request->estado,
                'pais' => $request->pais,
                'status' => 'alteracao',
                'data_alteracao' => date('Y-m-d')
            ]);


        $documentosJaSalvosNaBaseAnteriormente = DB::connection('integracao')
            ->table('lecom_apex_documentos_fornecedores')
            ->select('id', 'documento')
            ->where('id_fornecedor', $idFornecedor)
            ->where('status', '!=', 'exclusao')
            ->get();


        if (session()->has('documentacao_fornecedores')) {

            $documentosEnviados = [];

            foreach (session('documentacao_fornecedores') as $documento) {
                $documentosEnviados[] = $documento->caminho;
            }

            foreach ($documentosJaSalvosNaBaseAnteriormente as $documento) {

                if (!in_array($documento->documento, $documentosEnviados)) {

                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_fornecedores')
                        ->where('id', $documento->id)
                        ->update([
                            'status' => 'exclusao',
                            'data_alteracao' => date('Y-m-d')
                        ]);
                }
            }
        }

        $this->salvarDocumentosNaBaseDeDados($idFornecedor, Auth::user()->cpf);

        $produtosJaCadastrados = DB::connection('integracao')
            ->table('lecom_apex_fornecedor_produtos_servicos')
            ->where('id_fornecedor', $idFornecedor)
            ->get()
            ->toArray();

        if (isset($request->produtos) && count($request->produtos) > 0) {

            if ($produtosJaCadastrados) {

                foreach ($produtosJaCadastrados as $produtoJaCadastrado) {

                    if (!in_array($produtoJaCadastrado->codigo, $request->produtos)) {

                        DB::connection('integracao')
                            ->table('lecom_apex_fornecedor_produtos_servicos')
                            ->where([
                                'id_fornecedor' => $idFornecedor,
                                'codigo' => $produtoJaCadastrado->codigo
                            ])->update([
                                'status' => 'exclusao',
                                'data_alteracao' => date('Y-m-d H:i:s')
                            ]);
                    }
                }
            }

            foreach ($request->produtos as $codigoProduto) {

                $produtoJaExiste = DB::connection('integracao')
                    ->table('lecom_apex_fornecedor_produtos_servicos')
                    ->where([
                        'id_fornecedor' => $idFornecedor,
                        'codigo' => $codigoProduto
                    ])->count();

                if ($produtoJaExiste) {
                    DB::connection('integracao')
                        ->table('lecom_apex_fornecedor_produtos_servicos')
                        ->where([
                            'id_fornecedor' => $idFornecedor,
                            'codigo' => $codigoProduto
                        ])->update([
                            'status' => 'inclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                } else {
                    DB::connection('integracao')
                        ->table('lecom_apex_fornecedor_produtos_servicos')
                        ->insert([
                            'id_fornecedor' => $idFornecedor,
                            'codigo' => $codigoProduto,
                            'status' => 'inclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                }
            }
        }

        Log::criarLog(Auth::user()->cpf, 'Fornecedor alterado com sucesso');

        return redirect()
            ->route('gerenciar-fornecedor')
            ->with('fornecedorCadastradoComSucesso', 'Fornecedor alterado com sucesso!');
    }

    public function destroy($idFornecedor, Request $request)
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where([
                'cpf_responsavel' => Auth::user()->cpf,
                'id' => $idFornecedor
            ])->first();

        if ($fornecedor) {
            session()->forget('documentacao_fornecedores');

            DB::connection('integracao')
                ->table('lecom_apex_fornecedores')
                ->where([
                    'cpf_responsavel' => $fornecedor->cpf_responsavel,
                    'id' => $idFornecedor
                ])->update([
                    'status' => 'exclusao',
                    'data_alteracao' => date('Y-m-d'),
                    'motivo_exclusao' => $request->motivo
                ]);

            Log::criarLog(Auth::user()->cpf, 'Fornecedor excluído com sucesso');

            session()->flash('fornecedorExcluidoComSucesso', 'Fornecedor excluído com sucesso!');

            return response()->json(true);
        }
    }

    /**
     * Validation rules
     * @return array
     */
    private function rules()
    {
        return [
            'nomeFantasia' => 'required',
            'razaoSocial' => 'required',
            'cnpj' => 'required',
            'inscricaoEstadual' => 'required',
            'ramoAtuacao' => 'required',
            'nomeRepresentante' => 'required',
            'emailRepresentante' => 'required',
            'telefone' => 'required',
            'cep' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'municipio' => 'required',
            'estado' => 'required',
            'pais' => 'required'
        ];
    }

    /**
     * Validation rules messages
     * @return array
     */
    private function messages()
    {
        return [
            'nomeFantasia.required' => 'O campo Nome Fantasia é obrigatório',
            'razaoSocial.required' => 'O campo Razão Social é obrigatório',
            'cnpj.required' => 'O campo CNPJ é obrigatório',
            'inscricaoEstadual.required' => 'O campo Inscrição Estadual é obrigatório',
            'ramoAtuacao.required' => 'O campo Ramo de atuação é obrigatório',
            'nomeRepresentante.required' => 'O campo Nome Representante é obrigatório',
            'emailRepresentante.required' => 'O campo E-mail do Representante é obrigatório',
            'telefone.required' => 'O campo Telefone é obrigatório',
            'cep.required' => 'O campo CEP é obrigatório',
            'endereco.required' => 'O campo Endereço é obrigatório',
            'bairro.required' => 'O campo Bairro é obrigatório',
            'municipio.required' => 'O campo Município é obrigatório',
            'estado.required' => 'O campo Estado é obrigatório',
            'pais.required' => 'O campo País é obrigatório',
        ];
    }
}
