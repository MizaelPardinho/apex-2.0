<?php

namespace App\Http\Controllers\ProcessosDeAquisicoes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessosDeAquisicoesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user) {

            Log::criarLog($user->cpf, 'Acesso a página Processos de aquisições');

            $fornecedor = DB::connection('integracao')
                ->table('lecom_apex_fornecedores')
                ->where('cpf_responsavel', $user->cpf)
                ->where('status', '!=', 'exclusao')
                ->whereNull('data_exclusao')
                ->first();

            if ($fornecedor) {

                $notificacoes = DB::connection('integracao')
                    ->table('lecom_apex_processos_aquisicoes_notificacao')
                    ->where('cnpj', $fornecedor->cnpj)
                    ->get();

                if ($notificacoes) {
                    foreach ($notificacoes as $notificacao) {
                        $notificacoesDoUsuario[$notificacao->processo_lecom] = $notificacao->notificar;
                    }
                }
            }
        }

        $languages = [
            'pt-br' => 'PT',
            'en' => 'EN',
            'es' => 'ES'
        ];

        return view('processos-de-aquisicoes.index', [
            'pagina' => "Processo de Licitação",
            'processos' => DB::connection('lecom')
                ->table('v_aquisicoes')
                ->select(
                    'numero',
                    'modalidade',
                    'objeto',
                    'data_inicio',
                    'data_final',
                    'status',
                    'processo_apex'
                )
                ->where('idioma', $languages[session('localex')] ?? 'PT')
                ->distinct('numero')
                ->get(),
            'notificacoes' => !empty($notificacoesDoUsuario) ? $notificacoesDoUsuario : null,
            'fornecedor' => !empty($fornecedor) ? $fornecedor : null
        ]);
    }

    public function carregarOfertas(Request $request)
    {
        Log::criarLog(Auth::user()->cpf, 'Acesso a ofertas do processo ' . $request->numeroProcesso . ' (Processos de aquisições)');

        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {
            $cotacao = DB::connection('lecom')
                ->table('v_apex_cotacao')
                ->where([
                    'cnpj' => $fornecedor->cnpj,
                    'processo_lecom_pai' => $request->numeroProcesso,
                ])->where('resultado', '!=', 'Desclassificado')
                ->where('resultado', '!=', '')->first();

            if (!empty($cotacao) && in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14, 15])) {
                $ofertas = DB::connection('integracao')
                    ->table('lecom_apex_cotacoes_produtos_servicos')
                    ->join(
                        'lecom_apex_fornecedores',
                        'lecom_apex_fornecedores.id',
                        '=',
                        'id_fornecedor'
                    )->select('processo_lecom', 'razao_social', 'valor', 'data')
                    ->where([
                        'processo_lecom_pai' => $request->numeroProcesso
                    ])->orderBy('valor')->orderBy('data')->get();

                foreach ($ofertas as $oferta) {

                    $ofertaDesclassificada = DB::connection('lecom')
                        ->table('v_apex_cotacao')
                        ->where([
                            'processo_lecom' => $oferta->processo_lecom,
                        ])->where('resultado', 'Desclassificado')->count();

                    if (!$ofertaDesclassificada) {
                        $ofertasFiltradas[] = $oferta;
                    }
                }
            }
        }

        return view('processos-de-aquisicoes.ofertas', [
            'pagina' => "Processo de Licitação",
            'ofertas' => $ofertasFiltradas ?? null,
            'cotacao' => $cotacao,
        ]);
    }

    public function carregarDocumentos(Request $request)
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog($user->cpf, 'Acesso a documentação do processo ' . $request->numeroProcesso . ' (Processos de aquisições)');
        }

        $documentos = DB::connection('lecom')
            ->table('v_aquisicoes')
            ->select('objeto', 'tipo_doc_just', 'anexo_doc_just', 'numero')
            ->where('numero', $request->numeroProcesso)
            ->get();

        return view('processos-de-aquisicoes.documentos', [
            'pagina' => "Processo de Licitação",
            'documentos' => $documentos,
            'objeto' => $documentos[0]->objeto
        ]);
    }

    public function carregarComunicados(Request $request)
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog(Auth::user()->cpf, 'Acesso a comunicados do processo ' . $request->numeroProcesso . ' (Processos de aquisições)');
        }

        $comunicados = DB::connection('lecom')
            ->table('v_aquisicoes_comun')
            ->where('numero', $request->numeroProcesso)
            ->get();

        return view('processos-de-aquisicoes.comunicados', [
            'pagina' => "Processo de Licitação",
            'comunicados' => !empty($comunicados) ? $comunicados : []
        ]);
    }

    public function downloadDocumento($documento, $numeroProcesso)
    {
        $user = Auth::user();

        if ($user) {
            $cpf = $user->cpf;
        }

        DB::connection('integracao')
            ->table('log_documentos_baixados')
            ->insert([
                'processo_lecom' => $numeroProcesso,
                'cpf_responsavel' => $cpf ?? '',
                'ip' => $_SERVER['REMOTE_ADDR'],
                'documento' => $documento,
                'servico' => 'processos-aquisicoes',
                'data' => date('Y-m-d H:i:s')
            ]);

        return Storage::download('processos_aquisicoes/' . $documento);
    }

    public function downloadComunicado($documento)
    {
        return Storage::download('apex_lecom_aquisicoes/' . $documento);
    }

    public function notificarUsuario(Request $request)
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        $notificacaoJaExiste = DB::connection('integracao')
            ->table('lecom_apex_processos_aquisicoes_notificacao')
            ->where([
                'cnpj' => $fornecedor->cnpj,
                'processo_lecom' => $request->numeroProcesso
            ])->count();

        $dados = [
            'cnpj' => $fornecedor->cnpj,
            'processo_lecom' => $request->numeroProcesso,
            'notificar' => $request->checkSerNotificado === "true" ? 1 : 0
        ];

        if ($notificacaoJaExiste) {
            DB::connection('integracao')
                ->table('lecom_apex_processos_aquisicoes_notificacao')
                ->where([
                    'cnpj' => $fornecedor->cnpj,
                    'processo_lecom' => $request->numeroProcesso
                ])->update($dados);
        } else {
            DB::connection('integracao')
                ->table('lecom_apex_processos_aquisicoes_notificacao')
                ->where([
                    'cnpj' => $fornecedor->cnpj,
                    'processo_lecom' => $request->numeroProcesso
                ])->insert($dados);
        }

        $numeroProcessoApex = DB::connection('lecom')
            ->table('v_aquisicoes')
            ->select(
                'processo_apex'
            )->where('numero', $request->numeroProcesso)->first();

        if ($dados['notificar']) {
            session()->flash('success', "Você será notificado sobre o processo Nº {$numeroProcessoApex->processo_apex}.");
            Log::criarLog(Auth::user()->cpf, 'Participar das notificações do processo ' . $numeroProcessoApex->processo_apex . ' (Processos de aquisições)');
        } else {
            session()->flash('success', "Você não será mais notificado sobre o processo Nº {$numeroProcessoApex->processo_apex}.");
            Log::criarLog(Auth::user()->cpf, 'Deixar de participar das notificações do processo ' . $numeroProcessoApex->processo_apex . ' (Processos de aquisições)');
        }

        return response()->json(true);
    }
}
