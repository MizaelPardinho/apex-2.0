<?php

namespace App\Http\Controllers\Legislacao;

use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LegislacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog($user->cpf, 'Acesso a página Legislação');
        }

        return view('legislacao.index', [
            'pagina' => "Legislação",
            'documentos' => DB::connection('lecom')
                ->table('v_apex_legislacao')
                ->get()
        ]);
    }

    public function downloadDocumento($documento)
    {
        $user = Auth::user();

        DB::connection('integracao')
            ->table('log_documentos_baixados')
            ->insert([
                'cpf_responsavel' => $user ? $user->cpf : '',
                'ip' => $_SERVER['REMOTE_ADDR'],
                'documento' => $documento,
                'servico' => 'legislacao',
                'data' => date('Y-m-d H:i:s')
            ]);

        return Storage::download('legislacao/' . $documento);
    }
}
