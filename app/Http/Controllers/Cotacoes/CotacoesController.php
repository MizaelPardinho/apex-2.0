<?php

namespace App\Http\Controllers\Cotacoes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Cotacao;
use App\Models\Log;
use App\Traits\HandleArquivos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CotacoesController extends Controller
{
    use HandleArquivos;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {
            $cotacoes = DB::connection('lecom')
                ->table('v_apex_cotacao')
                ->select(
                    'processo_lecom_pai',
                    'processo_lecom',
                    'objeto',
                    'moedas',
                    'resultado',
                    'motivo',
                    'item',
                    'data_fim_cotacao',
                    'data_inicio_propostas',
                    'data_fim_propostas',
                    'data_fim_diligencia',
                    'modalidade',
                    'processo_apex',
                    'etapa_processo_lecom',
                    'etapa_lecom_pai'
                )
                ->where('cnpj', $fornecedor->cnpj)
                ->groupBy('processo_lecom')
                ->get();

            if ($cotacoes) {
                foreach ($cotacoes as $cotacao) {
                    $cotacoesJaEnviadasPorNumeroProcesso = DB::connection('integracao')
                        ->table('lecom_apex_cotacoes_produtos_servicos')
                        ->where([
                            'id_fornecedor' => $fornecedor->id,
                            'processo_lecom' => $cotacao->processo_lecom
                        ])->count();

                    if ($cotacoesJaEnviadasPorNumeroProcesso) {
                        $cotacoesJaEnviadas[$cotacao->processo_lecom] = $cotacao->processo_lecom;
                    }

                    // $this->criarOuAlterarNotificacao($fornecedor->id, $cotacao);
                }

                // $notificacoesNaoLidas = DB::connection('integracao')
                //     ->table('lecom_apex_notificacoes_cotacoes')
                //     ->where([
                //         'id_fornecedor' => $fornecedor->id,
                //         'lida' => 0
                //     ])->get();
            }
        }

        Log::criarLog(Auth::user()->cpf, 'Acesso a página Cotações');

        return view('cotacoes.index', [
            'pagina' => "Cotação",
            'cotacoes' => !empty($cotacoes) ? $cotacoes : null,
            'fornecedor' => !empty($fornecedor) ? $fornecedor : null,
            'cotacoesJaEnviadas' => !empty($cotacoesJaEnviadas) ? $cotacoesJaEnviadas : null,
            // 'notificacoesNaoLidas' => !empty($notificacoesNaoLidas) ? $notificacoesNaoLidas : null
        ]);
    }

    private function criarOuAlterarNotificacao($idFornecedor, $cotacao): void
    {
        $existeNotificacaoComMesmoNumeroDoProcesso = DB::connection('integracao')
            ->table('lecom_apex_notificacoes_cotacoes')
            ->where([
                'id_fornecedor' => $idFornecedor,
                'processo_lecom' => $cotacao->processo_lecom
            ])->count();

        if ($existeNotificacaoComMesmoNumeroDoProcesso) {
            DB::connection('integracao')
                ->table('lecom_apex_notificacoes_cotacoes')
                ->where([
                    'id_fornecedor' => $idFornecedor,
                    'processo_lecom' => $cotacao->processo_lecom
                ])->update([
                    'resultado' => $cotacao->resultado,
                    'motivo' => $cotacao->motivo
                ]);
        } else {
            DB::connection('integracao')
                ->table('lecom_apex_notificacoes_cotacoes')
                ->insert([
                    'id_fornecedor' => $idFornecedor,
                    'processo_lecom' => $cotacao->processo_lecom,
                    'resultado' => $cotacao->resultado,
                    'motivo' => $cotacao->motivo
                ]);
        }
    }

    public function carregarCotacao(Request $request)
    {
        $cotacao = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->select(
                'processo_lecom_pai',
                'processo_lecom',
                'discriminacao_item',
                'quantidade',
                'unidade_fornecimento',
                'objeto',
                'moedas',
                'item',
                'resultado',
                'ciclo_processo_lecom'
            )->whereIn(
                'resultado',
                ['Cotação aberta', 'Classificado Oferta Final', 'Diligência']
            )->where('processo_lecom', $request->numeroProcesso)->get();

        $cotacaoJaEnviada = DB::connection('integracao')
            ->table('lecom_apex_cotacoes_produtos_servicos')
            ->where([
                'id_fornecedor' => $request->idFornecedor,
                'processo_lecom' => $request->numeroProcesso,
                'cotacao_atual' => 1
            ])->first();

        return view('cotacoes.produtos', [
            'pagina' => "Cotação",
            'cotacao' => !empty($cotacao) ? $cotacao : null,
            'valorCotacaoEnviada' => !empty($cotacaoJaEnviada->valor) ? $cotacaoJaEnviada->valor : null,
            'observacoes' => !empty($cotacaoJaEnviada) ? $cotacaoJaEnviada->observacoes : []
        ]);
    }

    public function salvarCotacaoProdutos(Request $request)
    {
        $processoComEnvioDeCotacaoPermitida = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->whereIn(
                'resultado',
                ['Cotação aberta', 'Classificado Oferta Final', 'Diligência']
            )->where('processo_lecom', $request->numeroProcesso)->count();

        if ($processoComEnvioDeCotacaoPermitida) {

            $ultimaCotacaoEnviada = DB::connection('integracao')
                ->table('lecom_apex_cotacoes_produtos_servicos')
                ->where([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom' => $request->numeroProcesso,
                    'cotacao_atual' => 1
                ])->first();

            if (!empty($ultimaCotacaoEnviada)) {
                if (str_price_american($request['cotacao'][0]['valor']) > $ultimaCotacaoEnviada->valor) {
                    return response()->json(false);
                }
            }

            DB::connection('integracao')
                ->table('lecom_apex_cotacoes_produtos_servicos')
                ->where([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom' => $request->numeroProcesso,
                ])->update([
                    'cotacao_atual' => 0
                ]);

            DB::connection('integracao')
                ->table('lecom_apex_cotacoes_produtos_servicos')
                ->insert([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom_pai' => $request['cotacao'][0]['numeroProcessoPai'],
                    'processo_lecom' => $request->numeroProcesso,
                    'ciclo_processo_lecom' => $request['cotacao'][0]['ciclo'],
                    'valor' => !empty($request['cotacao'][0]['valor']) ? str_price_american($request['cotacao'][0]['valor']) : '',
                    'observacoes' => $request['cotacao'][0]['observacoes'],
                    'item' => $request['cotacao'][0]['item'],
                    'cotacao_atual' => 1,
                    'diligencia' => $request['cotacao'][0]['diligencia'],
                    'data' => date('Y-m-d H:i:s')
                ]);

            $fornecedor = DB::connection('integracao')
                ->table('lecom_apex_fornecedores')
                ->where('id', $request->idFornecedor)
                ->first();

            if ($fornecedor) {

                $numeroApex = DB::connection('lecom')
                    ->table('v_apex_cotacao')
                    ->select('processo_apex')
                    ->where('processo_lecom', $request->numeroProcesso)
                    ->first();

                Mail::to($fornecedor->email_representante)->send(new Cotacao($numeroApex->processo_apex));
            }

            Log::criarLog(Auth::user()->cpf, 'Enviou cotação para o processo Nº ' . $numeroApex->processo_apex);
            session()->flash('message', 'Cotação enviada com sucesso!');
        }

        return response()->json(true);
    }

    public function salvarDocumentosNaBaseDeDados(Request $request)
    {
        $documentosJaSalvosNaBaseAnteriormente = DB::connection('integracao')
            ->table('lecom_apex_documentos_cotacoes')
            ->select('id', 'documento')
            ->where('processo_lecom', $request->numeroProcesso)
            ->where('status', '!=', 'exclusao')
            ->get();


        if (session()->has('documentacao_cotacoes')) {

            $documentosEnviados = [];

            foreach (session('documentacao_cotacoes') as $documento) {
                $documentosEnviados[] = $documento->caminho;
            }

            foreach ($documentosJaSalvosNaBaseAnteriormente as $documento) {

                if (!in_array($documento->documento, $documentosEnviados)) {

                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_cotacoes')
                        ->where('id', $documento->id)
                        ->update([
                            'status' => 'exclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                }
            }
        }

        if (session()->has('documentacao_cotacoes')) {

            foreach (session('documentacao_cotacoes') as $dados) {

                $documentoJaExiste = DB::connection('integracao')
                    ->table('lecom_apex_documentos_cotacoes')
                    ->where([
                        'processo_lecom' => $request->numeroProcesso,
                        'documento' => $dados->caminho
                    ])->count();

                if (!$documentoJaExiste) {
                    $ciloCotacao = DB::connection('lecom')
                        ->table('v_apex_cotacao')
                        ->select('ciclo_processo_lecom')
                        ->where('processo_lecom', $request->numeroProcesso)
                        ->orderBy('ciclo_processo_lecom', 'DESC')->first();

                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_cotacoes')
                        ->insert([
                            'processo_lecom' => $request->numeroProcesso,
                            'ciclo_processo_lecom' => $ciloCotacao->ciclo_processo_lecom,
                            'descricao' => $dados->descricao,
                            'documento' => $dados->caminho,
                            'status' => 'inclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                }
            }

            Log::criarLog(Auth::user()->cpf, 'Salvando documentos referente ao processo Nº ' . $request->numeroProcesso);

            session()->forget("documentacao_cotacoes");
            session()->flash('message', 'Documento salvo com sucesso!');
        }

        return response()->json(true);
    }

    public function carregarDocumentos(Request $request)
    {
        $cotacao = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->whereIn(
                'resultado',
                ['Cotação aberta', 'Classificado Oferta Final', 'Diligência']
            )->where('processo_lecom', $request->numeroProcesso)->get();

        if (count($cotacao) > 0) {
            $documentos = DB::connection('integracao')
                ->table('lecom_apex_documentos_cotacoes')
                ->where([
                    'processo_lecom' => $request->numeroProcesso,
                    'status' => 'inclusao'
                ])->get();

            if ($documentos) {
                session()->forget('documentacao_cotacoes');

                foreach ($documentos as $documento) {
                    session()->push('documentacao_cotacoes', (object) [
                        'descricao' => $documento->descricao,
                        'arquivo' => $documento->documento,
                        'caminho' => $documento->documento
                    ]);
                }
            }

            return view('cotacoes.tabela-documentos-enviados', [
                'pagina' => "Cotação"
            ]);
        } else {
            return response()->json(false);
        }
    }

    public function carregarNotificacoes()
    {

        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {
            $notificacoesNaoLidas = DB::connection('integracao')
                ->table('lecom_apex_notificacoes_cotacoes')
                ->where([
                    'id_fornecedor' => $fornecedor->id,
                    'lida' => 0
                ])->get();
        }

        Log::criarLog(Auth::user()->cpf, 'Acesso a notificações');

        return view('cotacoes.notificacoes', [
            'pagina' => "Cotação",
            'notificacoes' => !empty($notificacoesNaoLidas) ? $notificacoesNaoLidas : []
        ]);
    }

    function marcarNotificacaoComoLida(Request $request)
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        $notificacaoAlterada = DB::connection('integracao')
            ->table('lecom_apex_notificacoes_cotacoes')
            ->where([
                'id_fornecedor' => $fornecedor->id,
                'processo_lecom' => $request->numeroProcesso
            ])->update([
                'lida' => 1
            ]);

        if ($notificacaoAlterada) {
            Log::criarLog(Auth::user()->cpf, 'Marcou a notificação como lida referente ao processo Nº ' . $request->numeroProcesso);

            return $this->carregarNotificacoes();
        }
    }

    function enviarEsclarecimento(Request $request)
    {
        $cotacao = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->whereIn(
                'resultado',
                ['Cotação aberta', 'Classificado Oferta Final', 'Diligência']
            )->where('processo_lecom', $request->numeroProcesso)->get();

        if (
            !empty($request->numeroProcessoPai)
            && !empty($request->numeroProcesso)
            && count($cotacao) > 0
        ) {
            if (!empty($request->arquivo)) {
                $user = Auth::user();
                $caminhoDocumento = remove_character_document($user->cpf) . '_' . Str::slug($request->descricao) . '_' . uniqid() . '.' . $request->arquivo->extension();

                $request->arquivo->storeAs(
                    'documentos_cotacoes_esclarecimentos',
                    $caminhoDocumento,
                    'ftp'
                );
            }

            DB::connection('integracao')
                ->table('lecom_apex_esclarecimentos')
                ->insert([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom_pai' => $request->numeroProcessoPai,
                    'processo_lecom' => $request->numeroProcesso,
                    'esclarecimento' => $request->esclarecimento,
                    'descricao_documento' => $request->descricao,
                    'documento' => isset($caminhoDocumento) ? $caminhoDocumento : '',
                    'data' => date('Y-m-d H:i:s')
                ]);

            Log::criarLog(
                Auth::user()->cpf,
                'Esclarecimento referente a cotação do processo Nº ' . $request->numeroProcesso
            );

            session()->flash('message', 'Esclarecimento enviado com sucesso!');

            return response()->json(true);
        }

        return response()->json(false);
    }
}
