<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class LocalizationLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = session("localex");

        $languages = ['pt-br', 'en', 'es'];

        if (in_array($locale, $languages)) {
            App::setLocale($locale);
        }

        return $next($request);
    }
}
