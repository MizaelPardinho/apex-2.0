<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnvioEsclarecimentoCotacao extends Mailable
{
    use Queueable, SerializesModels;

    private $numeroProcesso;
    private $esclarecimento;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($numeroProcesso, $esclarecimento)
    {
        $this->numeroProcesso = $numeroProcesso;
        $this->esclarecimento = $esclarecimento;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.envio-esclarecimento-cotacao', [
            'numeroProcesso' => $this->numeroProcesso,
            'esclarecimento' => $this->esclarecimento
        ])->subject('Esclarecimento');
    }
}
