<?php

namespace App\Support;

use Illuminate\Support\Facades\Storage;

class ArquivoSupport
{
    private $pasta;
    private $caminho;
    private $driver;

    public function __construct(string $pasta, string $caminho, string $driver)
    {
        $this->pasta = $pasta;
        $this->caminho = $caminho;
        $this->driver = $driver;
    }

    public function enviarArquivoParaServidor(object $arquivo): bool
    {
        return $arquivo->storeAs(
            $this->pasta,
            $this->caminho,
            $this->driver
        );
    }

    public function removerArquivoDoServidor(string $caminho): bool
    {
        return Storage::disk($this->driver)
            ->delete($this->pasta . "/" . $caminho);
    }

    public function salvarArquivoNaSessao(string $nomeSessao, object $dados): void
    {
        session()->push($nomeSessao, $dados);
    }

    public function removerArquivoNaSessao(string $nomeSessao, string $descricaoArquivo): void
    {
        $sessao = session()->get($nomeSessao);

        if (!empty($sessao)) {

            foreach ($sessao as $key => $dados) {

                if (in_array($descricaoArquivo, (array) $dados)) {

                    unset($sessao[$key]);
                    session()->forget($nomeSessao);
                    session()->put($nomeSessao, $sessao);
                }
            }
        }
    }

    public static function verificarSeArquivoJaExisteNaSessao(
        $nomeSessao,
        $descricaoArquivo,
        $verificarDescricaoArquivoJaIncluidoNaSessao
    ) {
        if (session()->has($nomeSessao) && $verificarDescricaoArquivoJaIncluidoNaSessao) {
            foreach (session()->get($nomeSessao) as $data) {
                if ($descricaoArquivo == $data->descricao) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function verificarArquivosExigidosNaSessao(string $nomeSessao, array $arquivosExigidos): array
    {
        $arquivosNaoEncontrados = [];
        $arquivosNaSessao = [];

        if (session()->has($nomeSessao)) {

            foreach (session($nomeSessao) as $arquivo) {
                array_push($arquivosNaSessao, $arquivo->descricao);
            }

            foreach ($arquivosExigidos as $arquivo) {

                if (!in_array($arquivo, $arquivosNaSessao)) {
                    array_push($arquivosNaoEncontrados, $arquivo);
                }
            }

            return $arquivosNaoEncontrados;
        }

        return $arquivosExigidos;
    }
}
