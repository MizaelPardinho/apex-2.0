<?php

/**
 * Converte problemas de acentuação
 *
 * @param string $string
 * @return string
 */
function convert_accentuation(string $string): string
{
    return utf8_encode(urldecode($string));
}

/**
 *
 * @param string $value
 * @return string
 */
function str_price_american(string $price): string
{
    return str_replace([".", ","], ["", "."], $price);
}

/**
 * Remove os caracteres especiais do CPF e CNPJ
 *
 * @param string $document
 * @return string
 */
function remove_character_document(string $document): string
{
    return str_replace(['.', '-', '/', '(', ')', ' '], '', $document);
}

function format_currency_brl(string $value): string
{
    return number_format($value, 2, ',', '.');
}


/**
 * Converte a data america em brasileira
 *
 * @param string $date
 * @return string
 */
function convert_date_br(string $date)
{
    return date('d/m/Y', strtotime($date));
}

/**
 * Retorna uma string somente contendo números
 * @param string $value
 * @return string
 */
function str_only_numbers(string $value): string
{
    return preg_replace("/[^0-9]/", "", $value);
}

function remove_accentuation($string)
{
    return strtolower(strtr(utf8_decode($string), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ '), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY-'));
}

function getEstados()
{
    return json_decode(file_get_contents('http://www.geonames.org/childrenJSON?geonameId=3469034'));
}

function verifyFile($file)
{
    $extensoesPermitidas = ["jpeg", "png", "jpg", "gif", "pdf"];
    $extensao = $file->extension();

    if (!in_array($extensao, $extensoesPermitidas)) {
        return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (jpeg, png, jpg, gif, pdf).'];
    }

    $tamanhoArquivo = $file->getClientSize();

    if ($tamanhoArquivo > 2048000) {
        return ['error' => 'O arquivo deve ter no máximo 2MB.'];
    }

    return true;
}

function formatCnpjCpf($value)
{
    $cnpj_cpf = preg_replace("/\D/", '', $value);

    if (strlen($cnpj_cpf) === 11) {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
    }

    return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
}

function formatTelefone($phone)
{
    $formatedPhone = preg_replace('/[^0-9]/', '', $phone);
    $matches = [];
    preg_match('/^([0-9]{2})([0-9]{4,5})([0-9]{4})$/', $formatedPhone, $matches);
    if ($matches) {
        return '(' . $matches[1] . ') ' . $matches[2] . '-' . $matches[3];
    }

    return $phone;
}
