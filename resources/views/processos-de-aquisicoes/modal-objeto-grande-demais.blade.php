<!-- The Modal -->
<div class="modal fade" id="modal-objeto-grande-demais">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h5>Objeto</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <!-- body content -->
            </div>
        </div>
    </div>
</div>
