<div class="row">
    <div class="col-lg-12">
        <p>
            <b>{{__('Objeto')}}:</b> {{ $objeto }}
        </p>

        <ul class="list-group">
            @foreach($documentos as $documento)

                @if (!empty($documento->anexo_doc_just) && !empty($documento->numero))
                    <li class="list-group-item">
                        <a href="{{ route('download-documento-processos-de-aquisicoes', ['documento' => $documento->anexo_doc_just, 'numeroProcesso' => $documento->numero]) }}" style="text-decoration: none">
                            <i class="fa fa-download"></i>
                            {{ $documento->tipo_doc_just }}
                        </a>
                    </li>
                @endif

            @endforeach
        </ul>
    </div>
</div>
