<!-- The Modal -->
<div class="modal fade" id="modal-ofertas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Ofertas do Processo Nº')}} <span class="numero-processo"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <!-- body content -->
            </div>
        </div>
    </div>
</div>
