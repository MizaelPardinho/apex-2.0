<div class="row">
    <div class="col-lg-12">
        @if (!empty($cotacao) && in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14, 15]))
            <div class="table-responsive" style="padding: 4px !important; margin-bottom: 8px;">
                <table class="table table-striped table-bordered dataTable dt-responsive w-100"
                    data-ordering="false" id="tabela-ofertas">

                    <thead>
                        <tr>
                            <th>{{__('Ordem')}}</th>

                            @if (in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14]))
                                <th>{{__('Fornecedor')}}</th>
                            @endif

                            <th>{{__('Valor Total')}}</th>

                            @if (in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14]))
                                <th>{{__('Data')}}</th>
                            @endif
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($ofertas as $oferta)
                            <tr>
                                <td>{{ $loop->iteration	}}</td>

                                @if (in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14]))
                                    <td>{{ $oferta->razao_social }}</td>
                                @endif

                                <td>{{ format_currency_brl($oferta->valor) }}</td>

                                @if (in_array($cotacao->etapa_lecom_pai, [5, 6, 11, 12, 14]))
                                    <td>{{ date('d/m/Y H:i', strtotime($oferta->data)) }}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p class="alert alert-info"><b>Atenção:</b> As ofertas não estão disponíveis nesse período.</p>
        @endif
    </div>
</div>
