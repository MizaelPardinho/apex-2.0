<div class="row">
    <div class="col-lg-12">
        @if (count($comunicados))
            @foreach($comunicados as $comunicado)
                <p>
                    <i class="fa fa-calendar"></i>
                    Data: {{ date('d/m/Y', strtotime($comunicado->data_comunicado)) }} às {{ $comunicado->hora_comunicado }}
                </p>

                <p>{{ $comunicado->texto_comunicado }}</p>

                @if (!empty($comunicado->Name_exp_2))
                    <p>
                        <a
                            href="{{ route('download-comunicado', $comunicado->Name_exp_2) }}"
                            style="text-decoration: none"
                        >
                            <i class="fa fa-download"></i>
                            {{ $comunicado->descricao_doc }}
                        </a>
                    </p>
                @endif

                <hr />
            @endforeach
        @else
            <p>Não há comunicados para este processo.</p>
        @endif
    </div>
</div>
