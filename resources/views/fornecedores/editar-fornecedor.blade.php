
@extends('layouts.layout-principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>Editar Fornecedor</h4>
                </div>

                <form method="POST" action="{{ route('editar-fornecedor', $fornecedor->id) }}" autocomplete="off">
                    @csrf

                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h5><i class="fa fa-user mt-3 mb-3"></i> {{__('Dados do Fornecedor')}}:</h5>

                                <div class="form-row">
                                    <div class="col-lg-4 form-group">
                                        <label for="cnpj">CNPJ:</label>
                                        <input type="text" name="cnpj" id="cnpj"
                                            class="form-control form-control-sm mask-cnpj {{ ($errors->has('cnpj') ? 'is-invalid' : '') }}"
                                            value="{{ old('cnpj') ?? $fornecedor->cnpj }}" />

                                            @if ($errors->has('cnpj'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('cnpj') }}
                                                </div>
                                            @endif
                                    </div>

                                    <div class="col-lg-8 form-group">
                                        <label for="razaoSocial">{{__('Razão Social')}}:</label>
                                        <input type="text" name="razaoSocial" id="razaoSocial"
                                            class="form-control form-control-sm {{ ($errors->has('razaoSocial') ? 'is-invalid' : '') }}"
                                            value="{{ old('razaoSocial') ?? $fornecedor->razao_social }}" maxlength="100" />

                                            @if ($errors->has('razaoSocial'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('razaoSocial') }}
                                                </div>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-lg-8 form-group">
                                        <label for="nomeFantasia">{{__('Nome Fantasia')}}:</label>
                                        <input type="text" name="nomeFantasia" id="nomeFantasia"
                                            class="form-control form-control-sm {{ ($errors->has('nomeFantasia') ? 'is-invalid' : '') }}"
                                            value="{{ old('nomeFantasia') ?? $fornecedor->nome_fantasia }}" maxlength="100" />

                                            @if ($errors->has('nomeFantasia'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('nomeFantasia') }}
                                                </div>
                                            @endif
                                    </div>

                                    <div class="col-lg-4 form-group">
                                        <label for="inscricaoEstadual">{{__('Inscrição Estadual')}}:</label>
                                        <input type="text" name="inscricaoEstadual" id="inscricaoEstadual"
                                            class="form-control form-control-sm {{ ($errors->has('inscricaoEstadual') ? 'is-invalid' : '') }}"
                                            value="{{ old('inscricaoEstadual') ?? $fornecedor->inscricao_estadual }}" maxlength="100" />

                                            @if ($errors->has('inscricaoEstadual'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('inscricaoEstadual') }}
                                                </div>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-lg-12 form-group">
                                        <label for="ramoAtuacao">{{__('Ramo de atuação')}}:</label>
                                        <input type="text" name="ramoAtuacao" id="ramoAtuacao"
                                            class="form-control form-control-sm {{ ($errors->has('ramoAtuacao') ? 'is-invalid' : '') }}"
                                            value="{{ old('ramoAtuacao') ?? $fornecedor->ramo_atuacao }}" maxlength="255" />

                                            @if ($errors->has('ramoAtuacao'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('ramoAtuacao') }}
                                                </div>
                                            @endif
                                    </div>

                                    <div class="col-lg-8 form-group">
                                        <label for="nomeRepresentante">{{__('Nome do Representante')}}:</label>
                                        <input type="text" name="nomeRepresentante" id="nomeRepresentante"
                                            class="form-control form-control-sm {{ ($errors->has('nomeRepresentante') ? 'is-invalid' : '') }}"
                                            value="{{ old('nomeRepresentante') ?? $fornecedor->nome_representante }}" maxlength="100" />

                                            @if ($errors->has('nomeRepresentante'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('nomeRepresentante') }}
                                                </div>
                                            @endif
                                    </div>

                                    <div class="col-lg-4 form-group">
                                        <label for="telefone">{{__('Telefone')}}:</label>
                                        <input type="text" name="telefone" id="telefone"
                                            class="form-control form-control-sm mask-cel {{ ($errors->has('telefone') ? 'is-invalid' : '') }}"
                                            value="{{ old('telefone') ?? $fornecedor->telefone }}" maxlength="20" />

                                            @if ($errors->has('telefone'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('telefone') }}
                                                </div>
                                            @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-lg-8 form-group">
                                        <label for="emailRepresentante">{{__('E-mail de contato')}}:</label>
                                        <input type="email" name="emailRepresentante" id="emailRepresentante"
                                            class="form-control form-control-sm {{ ($errors->has('emailRepresentante') ? 'is-invalid' : '') }}"
                                            value="{{ old('emailRepresentante') ?? $fornecedor->email_representante }}" maxlength="100" />

                                            @if ($errors->has('emailRepresentante'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('emailRepresentante') }}
                                                </div>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                @include('fornecedores.endereco')
                            </div>

                            <div class="col-lg-12">
                                <hr />
                            </div>

                            <div class="col-lg-6">
                                @include('fornecedores.adicionar-remover-produtos')
                            </div>

                            <div class="col-lg-6">
                                @include('fornecedores.envio-documentos')
                            </div>

                            <div class="col-lg-12 mb-3 mt-3">
                                <hr />

                                <h5><i class="fa fa-file"></i> {{__('Documentos Adicionados')}}:</h5>
                            </div>

                            <div class="col-lg-12">
                                <div class="conteudo_documentacao">
                                    @include('fornecedores.tabela-documentos-enviados')
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success mb-1">{{__('Salvar')}}</button>
                        <a href="{{ route('gerenciar-fornecedor') }}" class="btn btn-danger mb-1">{{__('Cancelar')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script src="{{ asset('js/handleArquivos/scripts.js') }}"></script>

    <script>
        $(function() {
            let load = $(".ajax_load");

            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            $('#cnpj').on('change', function() {
                let cnpj = $(this).val();

                if (!validarCNPJ(cnpj)) {
                    alert('Atenção: CNPJ inválido!');
                    $(this).val('');
                    $(this).focus();
                    return;
                }

                $.ajax({
                    url: "{{ route('carregar-informacoes-apartir-cnpj-fornecedor') }}",
                    method: "POST",
                    data: { cnpj },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#razaoSocial').val(response.nome);
                        $('#nomeFantasia').val(response.fantasia);
                        $('#cep').val(response.cep).trigger('change');
                        $('#emailRepresentante').val(response.email);
                    },
                });
            });

            $('.select2').select2({
                placeholder: "Clique aqui para adicionar",
                closeOnSelect: false
            });

            enviarDocumento({
                tamanhoAnexoPermitido: 5048000,
                servidor: 'ftp',
                pastaServidor: 'documentos_fornecedores',
                nomeSessao: 'documentacao_fornecedores',
                caminhoTabela: 'fornecedores.tabela-documentos-enviados',
                rota: "{{ route('salvar-documentos-fornecedores') }}",
                idTabela: 'documentacao_fornecedores'
            });

            removerDocumento({
                servidor: 'ftp',
                pastaServidor: 'documentos_fornecedores',
                nomeSessao: 'documentacao_fornecedores',
                caminhoTabela: 'fornecedores.tabela-documentos-enviados',
                rota: "{{ route('remover-documentos-fornecedores') }}",
                idTabela: 'documentacao_fornecedores'
            });
        });
    </script>
@endsection
