<h5><i class="fa fa-map-marker mt-3 mb-3"></i> {{__('Endereço')}}:</h5>
<div class="form-row">
    <div class="col-lg-4 form-group">
        <label for="cep">{{__('CEP')}}:</label>
        <input type="text" name="cep" id="cep" onChange="pesquisaCep(this.value);"
            class="form-control form-control-sm mask-cep {{ ($errors->has('cep') ? 'is-invalid' : '') }}"
            value="{{ old('cep') ? old('cep') : (!empty($fornecedor) ? $fornecedor->cep : '') }}" />

            @if ($errors->has('cep'))
                <div class="invalid-feedback">
                    {{ $errors->first('cep') }}
                </div>
            @endif
    </div>

    <div class="col-lg-8 form-group">
        <label for="endereco">{{__('Endereço')}}:</label>
        <input type="text" name="endereco" id="endereco"
            class="form-control form-control-sm {{ ($errors->has('endereco') ? 'is-invalid' : '') }}"
            maxlength="255"
            value="{{ old('endereco') ? old('endereco') : (!empty($fornecedor) ? $fornecedor->endereco : '') }}" />

            @if ($errors->has('endereco'))
                <div class="invalid-feedback">
                    {{ $errors->first('endereco') }}
                </div>
            @endif
    </div>
</div>

<div class="form-row">
    <div class="col-lg-4 form-group">
        <label for="bairro">{{__('Bairro')}}:</label>
        <input type="text" name="bairro" id="bairro"
            class="form-control form-control-sm {{ ($errors->has('bairro') ? 'is-invalid' : '') }}"
            maxlength="100"
            value="{{ old('bairro') ? old('bairro') : (!empty($fornecedor) ? $fornecedor->bairro : '') }}" />

            @if ($errors->has('bairro'))
                <div class="invalid-feedback">
                    {{ $errors->first('bairro') }}
                </div>
            @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="municipio">{{__('Município')}}:</label>
        <input type="text" name="municipio" id="municipio"
            class="form-control form-control-sm {{ ($errors->has('municipio') ? 'is-invalid' : '') }}"
            maxlength="100"
            value="{{ old('municipio') ? old('municipio') : (!empty($fornecedor) ? $fornecedor->municipio : '') }}" />

            @if ($errors->has('municipio'))
                <div class="invalid-feedback">
                    {{ $errors->first('municipio') }}
                </div>
            @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="estado">{{__('Estado')}}:</label>
        <select name="estado" id="estado" class="form-control form-control-sm {{ ($errors->has('estado') ? 'is-invalid' : '') }}">
            <option></option>

            @foreach(getEstados()->geonames as $estado)
                <option value="{{ $estado->adminCodes1->ISO3166_2 }}"
                    {{
                        old('estado') == $estado->adminCodes1->ISO3166_2
                        ? 'selected'
                        : (!empty($fornecedor) && $fornecedor->estado == $estado->adminCodes1->ISO3166_2
                        ? 'selected'
                        : '' ) }}>
                    {{ $estado->name }}
                </option>
            @endforeach
        </select>

        @if ($errors->has('estado'))
            <div class="invalid-feedback">
                {{ $errors->first('estado') }}
            </div>
        @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="pais">{{__('País')}}:</label>
        <input type="text" name="pais" id="pais"
            class="form-control form-control-sm {{ ($errors->has('pais') ? 'is-invalid' : '') }}"
            maxlength="100"
            value="{{ old('pais') ? old('pais') : (!empty($fornecedor) ? $fornecedor->pais : '') }}" />

            @if ($errors->has('pais'))
                <div class="invalid-feedback">
                    {{ $errors->first('pais') }}
                </div>
            @endif
    </div>
</div>
