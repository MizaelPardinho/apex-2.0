<h5><i class="fa fa-shopping-cart mt-3 mb-3"></i> {{__('Adicionar/Remover Produtos e Serviços')}}:</h5>
<div class="form-row">
    <div class="col-lg-12 form-group">
        <label for="produtos">{{__('Produtos e Serviços')}}:</label>
        <select name="produtos[]" id="produtos"
            class="form-control select2"
            multiple>

            @foreach($produtos as $produto)
                <option value="{{ $produto->codigo }}"

                    @if (!empty($produtosJaSelecionados[$produto->codigo]))
                        {{ ($produto->codigo == $produtosJaSelecionados[$produto->codigo]->codigo ? 'selected' : '' ) }}
                    @endif

                    @if (!empty(session('produtosJaSelecionados')[0][$produto->codigo]))
                        {{ ($produto->codigo == session('produtosJaSelecionados')[0][$produto->codigo] ? 'selected' : '' ) }}
                    @endif
                >
                    {{ $produto->codigo }} - {{ $produto->nome }}
                </option>
            @endforeach
        </select>
    </div>
</div>
