<h5><i class="fa fa-folder-open mt-3 mb-3"></i> {{__('Envio de Documentos')}}:</h5>

<div class="form-row">
    <div class="col-lg-8 form-group">
        <label for="descricao_documento">{{__('Descrição do Documento')}}:</label>
        <input type="text" name="descricao_documento" id="descricao_documento"
            class="form-control form-control-sm" />
    </div>

    <div class="col-lg-12 form-group">
        <label class="btn btn-primary">
            <i class="fa fa-image"></i> {{__('Escolher Arquivo')}} <input type="file" style="display: none;" name="documento" id="documento">
        </label>
        <label id="mensagem_arquivo">{{__('Nenhum Arquivo Selecionado')}}</label>
    </div>

    <div class="col-lg-12">
        <button class="btn btn-primary btn-enviar-documento">
            {{__('Adicionar Documento')}}
        </button>
    </div>
</div>

<script>
    $('#documento').change(function() {
         $('#mensagem_arquivo').html('<b>{{__("Arquivo Selecionado")}}:</b> ' + $(this).val());
    });
</script>