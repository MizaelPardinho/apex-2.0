<div class="modal fade" id="modal_login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-lock"></i> {{__('Entre na sua conta')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form name="formularioLogin" action="{{ route('validar-usuario') }}" id="frmCpf" method="post">
                    @csrf

                    <label for="cpf_login">{{__('Digite aqui seu CPF')}}:</label>
                    <div class="form-group">
                        <input type="text" id="cpf_login" name="cpf_login" class="form-control mask-cpf">
                    </div>

                    <button class="btn btn-primary" id="valida_cpf_login" type="submit">
                        <i class="fa fa-lock"></i> Login
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("#valida_cpf_login").click(function (e) {

        e.preventDefault();

        if ($("#cpf_login").val().length == 14) {

            if (validarCPF($("#cpf_login").val())) {
                $("#frmCpf").submit();
            } else {
                alert("CPF Inválido! Informe corretamente o número do seu CPF.");
                $("#cpf_login").val("");
                $("#cpf_login").focus();
            }
        } else {
            alert("Por favor preencha o CPF com 11 dígitos.");
            $("#cpf_login").val("");
            $("#cpf_login").focus();
        }
    });
</script>
