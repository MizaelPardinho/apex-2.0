<!-- The Modal -->
<div class="modal fade" id="modal-exigencias-apex" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Confirmação</h4>
            </div>

            <div class="modal-body">
                <div class="form-check mb-3">
                    <label class="form-check-label">
                        <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                        Possuo conhecimento da legislação vigente, mormente quanto à não
                        existência de fato impeditivo de sua participação no processo
                    </label>
                </div>

                <div class="form-check mb-3">
                    <label class="form-check-label">
                        <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                        Estou ciente das regras de uso do Portal de Aquisições
                    </label>
                </div>

                <div class="form-check mb-3">
                    <label class="form-check-label">
                        <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                        Sou integralmente responsável pelo uso correto e adequado das
                        minhas credenciais de acesso
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                        Estou em dia com as obrigações fiscais e sociais exigidas em
                        legislação para participar de um processo de aquisição
                    </label>
                </div>

                <button class="btn btn-primary btn-concordo-exigencias mt-3">Concordo</button>
                <button class="btn btn-danger btn-cancelar-exigencias mt-3">Cancelar</button>
            </div>
        </div>
    </div>
</div>
