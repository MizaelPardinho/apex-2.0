@extends('layouts.layout-principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">

                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                <div class="card card-servicos">
                    <div class="card-header card-header-title">{{__('Fale Conosco - Formulário para Contato com a Coordenação de Aquisições')}}</div>
                    <div class="card-body py-4">
                        <form method="POST" action="{{ route('enviar-mensagem') }}">

                            @csrf

                            <div class="form-row">
                                <div class="col-lg-6 form-group">
                                    <label for="nome">{{__('Nome')}}:</label>
                                    <input
                                        maxlength="40"
                                        type="text"
                                        name="nome"
                                        class="form-control form-control-sm {{ ($errors->has('nome') ? 'is-invalid' : '') }}"
                                        id="nome"
                                        value="{{ old('nome') }}"
                                    />

                                    @if ($errors->has('nome'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('nome') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="email">{{__('E-mail')}}:</label>
                                    <input
                                        maxlength="100"
                                        type="email"
                                        class="form-control form-control-sm {{ ($errors->has('email') ? 'is-invalid' : '') }}"
                                        name="email"
                                        id="email"
                                        value="{{ old('email') }}"
                                    />

                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-2 form-group">
                                    <label for="telefone">{{__('Telefone')}}:</label>
                                    <input
                                        type="text"
                                        class="form-control form-control-sm mask-cel {{ ($errors->has('telefone') ? 'is-invalid' : '') }}"
                                        name="telefone"
                                        id="telefone"
                                        value="{{ old('telefone') }}"
                                    />

                                    @if ($errors->has('telefone'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('telefone') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-2 form-group">
                                    <label for="assunto">{{__('Assunto')}}:</label>
                                    <select id="assunto" name="assunto" class="form-control form-control-sm {{ ($errors->has('assunto') ? 'is-invalid' : '') }}">
                                        <option></option>
                                        <option value="Elogio" {{ old('assunto') === 'Elogio' ? 'selected' : '' }}>{{__('Elogio')}}</option>
                                        <option value="Dúvida" {{ old('assunto') === 'Dúvida' ? 'selected' : '' }}>{{__('Dúvida')}}</option>
                                        <option value="Reclamação" {{ old('assunto') === 'Reclamação' ? 'selected' : '' }}>{{__('Reclamação')}}</option>
                                    </select>

                                    @if ($errors->has('assunto'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('assunto') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-2 form-group">
                                    <label for="numero_processo">{{__('Nº Processo')}}: (opcional)</label>
                                    <select id="numero_processo" name="numero_processo" class="form-control form-control-sm">
                                        <option></option>

                                        @foreach($processosAquisicoes as $processo)
                                            <option {{ old('numero_processo') == $processo->processo_apex ? 'selected' : '' }}>
                                                {{ $processo->processo_apex }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="empresa">{{__('Empresa')}}: (opcional)</label>
                                    <input
                                        maxlength="40"
                                        type="text"
                                        name="empresa"
                                        class="form-control form-control-sm"
                                        id="empresa"
                                        value="{{ old('empresa') }}"
                                    />
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-12 form-group">
                                    <label for="mensagem">{{__('Mensagem')}}:</label>
                                    <textarea
                                        class="form-control {{ ($errors->has('mensagem') ? 'is-invalid' : '') }}"
                                        id="mensagem"
                                        name="mensagem"
                                        rows="4"
                                        placeholder="{{__('Por favor, escreva a sua mensagem')}}"
                                    ></textarea>

                                    @if ($errors->has('mensagem'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('mensagem') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-12">
                                    {!! $showRecaptcha !!}
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-12 form-group">
                                    <button id="btn-enviar-mensagem" class="btn btn-large btn-primary" type="submit" disabled>
                                        <i class="fa fa-envelope"></i> {{__('Enviar Mensagem')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('post-script')
    <script type="text/javascript">

        $("#btn-enviar-mensagem").prop('disabled', true);

        jQuery(document).ready(function(){
            jQuery('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
            jQuery('.g-recaptcha').attr('data-expired-callback', 'onReCaptchaTimeOut');
        });

        function onReCaptchaTimeOut(){
            $("#btn-enviar-mensagem").prop('disabled', true);
        }
        function onReCaptchaSuccess(){
            $("#btn-enviar-mensagem").prop('disabled', false);
        }
    </script>
@endsection
