<!-- The Modal -->
<div class="modal fade" id="modal-esclarecimento">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Esclarecimento do processo Nº')}} <span class="numero-processo"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row envioEsclarecimentoBloqueado d-none">
                    <div class="col-lg-12">
                        <p class="alert alert-info">
                            Atenção: A cotação foi encerrada.
                        </p>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="esclarecimento">Esclarecimento: </label>
                        <textarea name="esclarecimento" id="esclarecimento"
                            class="form-control form-control-sm" cols="5" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-5 form-group">
                        <label for="descricao_documento_esclarecimento">
                            {{__('Descrição do Documento')}}:
                        </label>

                        <input
                            type="text"
                            name="descricao_documento"
                            id="descricao_documento_esclarecimento"
                            class="form-control form-control-sm"
                            maxlength="100"
                        />
                    </div>

                    <div class="col-lg-5 form-group mt-2">
                        <label class="btn btn-primary" style="margin-top: 18px !important;">
                            <i class="fa fa-image"></i>
                            {{__('Escolher Arquivo')}}
                            <input
                                type="file"
                                style="display: none;"
                                name="documento_esclarecimento"
                                id="documento_esclarecimento"
                            />
                        </label>

                        <div id="mensagem_arquivo_selecionado">
                            {{__('Nenhum Arquivo Selecionado')}}
                        </div>
                    </div>
                </div>

                <button class="btn btn-success mt-3 enviar-esclarecimento">
                    Enviar <i class="fa fa-send"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#documento_esclarecimento').change(function() {
        $('#mensagem_arquivo_selecionado').html('<b>{{__("Arquivo Selecionado")}}:</b> ' + $(this).val());
    });
</script>
