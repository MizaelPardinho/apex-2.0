@extends('layouts.layout-principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>{{__('Cotações')}}</h4>
                </div>

                <div class="card-body">
                    {{-- <button class="btn btn-primary text-white mb-3 btn-notificacoes">
                        {{__('Notificações')}}
                        <span class="badge badge-light qtd-notificacoes">{{ !empty($notificacoesNaoLidas) ? count($notificacoesNaoLidas) : 0 }}</span>
                    </button> --}}

                    <div class="table-responsive py-1 pl-1 pr-1">
                        <table class="table table-striped table-bordered dataTable dt-responsive w-100">
                            <thead>
                                <tr>
                                    <th width="5%">{{__('Nº Processo Eletrônico')}}</th>
                                    <th width="5%">{{__('Nº Aquisição')}}</th>
                                    <th>{{__('Moeda')}}</th>
                                    <th>{{__('Modalidade')}}</th>
                                    <th>{{__('Resultado')}}</th>
                                    <th>{{__('Situação')}}</th>
                                    <th data-priority="3">{{__('Limite envio cotação')}}</th>
                                    <th data-priority="1">{{__('Início lances BAFO')}}</th>
                                    <th data-priority="2">{{__('Início período randômico')}}</th>
                                    <th width="15%" data-priority="4">{{__('Ações')}}</th>
                                    <th>{{__('Objeto')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($cotacoes)
                                    @foreach($cotacoes as $cotacao)
                                        <tr>
                                            <td>{{ $cotacao->processo_apex }}</td>
                                            <td>{{ $cotacao->processo_lecom }}</td>
                                            <td>{{ $cotacao->moedas }}</td>
                                            <td>{{ $cotacao->modalidade }}</td>
                                            <td>{{ $cotacao->resultado }}</td>
                                            <td>
                                                {{
                                                    !empty($cotacoesJaEnviadas[$cotacao->processo_lecom]) ?
                                                    __("Enviado")
                                                    : __("Não Enviado")
                                                }}
                                            </td>
                                            <td>{{ $cotacao->data_fim_cotacao }}</td>
                                            <td>{{ $cotacao->modalidade === 'Contratação Direta' ? $cotacao->data_inicio_propostas : 'Não Aplicável' }}</td>
                                            <td>{{ $cotacao->modalidade === 'Contratação Direta' ? $cotacao->data_fim_propostas : 'Não Aplicável' }}</td>
                                            <td>
                                                @php
                                                    $dataAtual = DateTime::createFromFormat('d/m/Y H:i', date('d/m/Y H:i'));
                                                @endphp

                                                @if (($cotacao->etapa_lecom_pai === 3 && $cotacao->etapa_processo_lecom === 4 && $dataAtual < DateTime::createFromFormat('d/m/Y H:i', $cotacao->data_fim_cotacao))
                                                    || ($cotacao->etapa_lecom_pai === 15 && $cotacao->etapa_processo_lecom === 9 && $dataAtual < DateTime::createFromFormat('d/m/Y H:i', $cotacao->data_fim_propostas))
                                                    || ($cotacao->etapa_processo_lecom === 7 && $dataAtual < DateTime::createFromFormat('d/m/Y H:i', $cotacao->data_fim_diligencia)))

                                                    <button class="btn btn-sm btn-primary btn-ofertar-valor mb-2 w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}"
                                                        data-moeda="{{ $cotacao->moedas }}"
                                                        data-fornecedor="{{ $fornecedor->id }}">
                                                        {{__('Ofertar Valor')}}
                                                    </button>

                                                    <button class="btn btn-sm btn-primary btn-abrir-modal-enviar-documento mb-2 w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}">
                                                        {{__('Enviar Documento')}}
                                                    </button>

                                                    <button class="btn btn-sm btn-primary btn-abrir-modal-esclarecimento w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}"
                                                        data-processopai={{ $cotacao->processo_lecom_pai }}
                                                        data-fornecedor="{{ $fornecedor->id }}">
                                                        {{__('Esclarecimento')}}
                                                    </button>
                                                @endif
                                            </td>
                                            <td>{{ $cotacao->objeto }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('cotacoes.modal-produtos-cotacao')
    @include('cotacoes.modal-envio-documentos')
    @include('cotacoes.modal-notificacoes')
    @include('cotacoes.modal-esclarecimento')
</div>

@endsection

@section('post-script')
    <script src="{{ asset('js/handleArquivos/scripts.js') }}"></script>

    <script>
        $(function() {
            let load = $(".ajax_load");

            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            $('body').on('click', '.btn-ofertar-valor', function() {

                const numeroProcesso = $(this).data('processo');
                const idFornecedor = $(this).data('fornecedor');
                const moeda = $(this).data('moeda');

                $.ajax({
                    url: "carregar-cotacao",
                    method: "POST",
                    data: { numeroProcesso, idFornecedor},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-produtos-cotacao')
                            .find('.modal-header .numero-processo')
                            .html(numeroProcesso);

                        $('#modal-produtos-cotacao')
                            .find('.modal-body')
                            .html(response);

                        if (moeda === 'BRL') {
                            $('.input-cotacoes').mask('000.000.000.000.000,00', { reverse: true });
                        } else {
                            $('.input-cotacoes').mask('000000000000000.00', { reverse: true });
                        }

                        recalcularDataTable('tabela-produtos-servicos');
                        $('#modal-produtos-cotacao').modal('show');
                    },
                });
            });

            $('body').on('click', '.salvar-cotacao', function() {

                let numeroProcesso = $('#modal-produtos-cotacao')
                    .find('.modal-header .numero-processo')
                    .html();

                let cotacao = [];

                cotacao.push({
                    numeroProcessoPai: $('input[name=cotacao]').data('processopai'),
                    item: $('input[name=cotacao]').data('item'),
                    diligencia: $('input[name=cotacao]').data('diligencia'),
                    valor: $('input[name=cotacao]').val(),
                    observacoes: $('#observacoes').val(),
                    ciclo: $('input[name=cotacao]').data('ciclo')
                });

                $.ajax({
                    url: "{{ route('salvar-cotacoes-produtos') }}",
                    method: "POST",
                    data: {
                        numeroProcesso,
                        cotacao,
                        idFornecedor: "{{ !empty($fornecedor->id) ? $fornecedor->id : '' }}"
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            $('.valor-nao-permitido').addClass('d-none');
                            location.reload();
                        } else {
                            $('.valor-nao-permitido').removeClass('d-none');
                        }
                    },
                });
            });

            $('body').on('click', '.btn-abrir-modal-enviar-documento', function() {
                const numeroProcesso = $(this).data('processo');

                $('body .btn-salvar-documentos').attr('data-processo', numeroProcesso);

                $.ajax({
                    url: "{{ route('carregar-documentos-cotacoes') }}",
                    method: "POST",
                    data: { numeroProcesso },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {

                        load.fadeOut(200);

                        if (response) {
                            $('#modal-envio-documentos-cotacao').find('.modal-body .envioDeDocumentosBloqueado').addClass('d-none')
                            $('#modal-envio-documentos-cotacao').find('.modal-body .envioDeDocumentosLiberado').removeClass('d-none')

                            $('.conteudo_documentacao').html(response);
                            recalcularDataTable('documentacao_cotacoes');

                        } else {
                            $('#modal-envio-documentos-cotacao').find('.modal-body .envioDeDocumentosBloqueado').removeClass('d-none')
                            $('#modal-envio-documentos-cotacao').find('.modal-body .envioDeDocumentosLiberado').addClass('d-none')
                        }

                        $('#modal-envio-documentos-cotacao').modal('show');
                    },
                });
            });

            enviarDocumento({
                tamanhoAnexoPermitido: 5048000,
                servidor: 'ftp',
                pastaServidor: 'documentos_cotacoes',
                nomeSessao: 'documentacao_cotacoes',
                caminhoTabela: 'cotacoes.tabela-documentos-enviados',
                rota: 'salvar-documentos-cotacoes',
                idTabela: 'documentacao_cotacoes'
            });

            removerDocumento({
                servidor: 'ftp',
                pastaServidor: 'documentos_cotacoes',
                nomeSessao: 'documentacao_cotacoes',
                caminhoTabela: 'cotacoes.tabela-documentos-enviados',
                rota: 'remover-documentos-cotacoes',
                idTabela: 'documentacao_cotacoes'
            });

            $('body').on('click', '.btn-salvar-documentos', function(e) {
                e.preventDefault();

                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "{{ route('salvar-documentos-cotacoes-na-base-de-dados') }}",
                    method: "POST",
                    data: { numeroProcesso },
                    dataType: 'json',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            $('#modal-envio-documentos-cotacao').modal('hide');
                            location.reload();
                        }
                    },
                });
            });

            $('body').on('click', '.btn-notificacoes', function(e) {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('carregar-notificacoes') }}",
                    method: "POST",
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-notificacoes').find('.modal-body').html(response);

                        $('#modal-notificacoes').modal('show');
                    },
                });
            });

            $('body').on('click', '.marcar-como-lida', function() {
                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "{{ route('marcar-notificacao-como-lida') }}",
                    method: "POST",
                    data: { numeroProcesso},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        let qtdNotificacoes = $('.qtd-notificacoes').html();

                        $('.qtd-notificacoes').html(Number(qtdNotificacoes) - 1);
                        $('#modal-notificacoes').find('.modal-body').html(response);
                    },
                });
            });

            $('body').on('click', '.btn-abrir-modal-esclarecimento', function() {
                const numeroProcessoPai = $(this).data('processopai');
                const numeroProcesso = $(this).data('processo');
                const idFornecedor = $(this).data('fornecedor');

                $('body .enviar-esclarecimento').attr('data-processopai', numeroProcessoPai);
                $('body .enviar-esclarecimento').attr('data-processo', numeroProcesso);
                $('body .enviar-esclarecimento').attr('data-fornecedor', idFornecedor);

                $('#modal-esclarecimento')
                    .find('.modal-header .numero-processo')
                    .html(numeroProcesso);

                $('#modal-esclarecimento').modal('show');
            });

            $('body').on('click', '.enviar-esclarecimento', function() {

                let arquivo = $("#documento_esclarecimento")[0].files[0];
                let descricao = $("#descricao_documento_esclarecimento").val();

                if (arquivo && !descricao) {
                    alert("Atenção: Digite a descrição do documento.");
                    $("#descricao_documento_esclarecimento").trigger('focus');
                    return false;
                }

                if (descricao && !arquivo) {
                    alert("Atenção: Escolha um arquivo ou apague a descrição caso não queira enviar nenhum arquivo.");
                    return false;
                }

                if (arquivo && !validarAnexo(arquivo, ["jpg", "jpeg", "png", "pdf"], 5048000)) {
                    return false;
                }

                if (!$('#esclarecimento').val()) {
                    alert('Atenção: O campo esclarecimento não pode ser vazio.');
                    $('#esclarecimento').focus();
                    return;
                }

                const formData = new FormData();

                if (arquivo) {
                    formData.append("arquivo", arquivo);
                }

                formData.append("descricao", descricao);
                formData.append("idFornecedor", $(this).data('fornecedor'));
                formData.append("numeroProcessoPai", $(this).data('processopai'));
                formData.append("numeroProcesso", $(this).data('processo'));
                formData.append("esclarecimento", $('#esclarecimento').val());

                $.ajax({
                    url: "{{ route('salvar-esclarecimento-cotacao') }}",
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            $('.envioEsclarecimentoBloqueado').addClass('d-none');
                            location.reload();
                        } else {
                            $('.envioEsclarecimentoBloqueado').removeClass('d-none');
                        }
                    },
                });
            });
        });
    </script>
@endsection
