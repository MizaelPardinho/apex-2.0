<div class="row">
    @if (count($cotacao) > 0)
        <div class="col-lg-12">
            @if ($cotacao[0]->resultado === 'Classificado')
                <div class="alert alert-info">
                    <b>Parabéns!</b> Texto para classificado...
                </div>
            @endif

            @if ($cotacao[0]->resultado === 'Diligência')
                <div class="alert alert-info">
                    <b>Diligência!</b> Texto para diligência...
                </div>
            @endif

            <div class="alert alert-danger valor-nao-permitido d-none">
                Atenção: Não é permitido enviar um valor maior do que o enviado na cotação anterior.
            </div>

            <p class="ml-1">
                <b>{{__('Objeto')}}: </b> {{ $cotacao[0]->objeto }}
            </p>

            <div class="table-responsive" style="padding: 4px !important; margin-bottom: 8px;">
                <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela-produtos-servicos">
                    <thead>
                        <tr>
                            <th>{{__('Discriminação')}}</th>
                            <th>{{__('Quantidade')}}</th>
                            <th>{{__('Unidade')}}</th>
                            <th>{{__('Moeda')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($cotacao as $item)
                            <tr>
                                <td>{{ $item->discriminacao_item }}</td>
                                <td>{{ $item->quantidade }}</td>
                                <td>{{ $item->unidade_fornecimento }}</td>
                                <td>{{ $item->moedas }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr class="bg-success text-white">
                            <td colspan="3" class="text-right"><b>Digite o valor total da cotação:</b></td>
                            <td>
                                <input
                                    type="text"
                                    name="cotacao"
                                    class="form-control form-control-sm input-cotacoes"
                                    data-processopai={{ $cotacao[0]->processo_lecom_pai }}
                                    data-item={{ $cotacao[0]->item }}
                                    data-processo="{{ $cotacao[0]->processo_lecom }}"
                                    data-diligencia="{{ $cotacao[0]->resultado === 'Diligência' ? 1 : 0 }}"
                                    data-ciclo="{{ $cotacao[0]->ciclo_processo_lecom }}"
                                    value="{{
                                        !empty($valorCotacaoEnviada) ?
                                        $valorCotacaoEnviada
                                        : ''
                                    }}"
                                />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <label class="ml-1" for="observacoes">Observações:</label>
            <textarea name="observacoes" id="observacoes"
                class="form-control form-control-sm mb-2 ml-1"
                maxlength="255" cols="5" rows="5">{{ !empty($observacoes) ? $observacoes : '' }}</textarea>

            <button type="button" class="btn btn-success salvar-cotacao mt-2 ml-1">
                {{__('Enviar Cotação')}} <i class="fa fa-send"></i>
            </button>
        </div>
    @else
        <div class="col-lg-12">
            <p class="alert alert-info">Atenção: A cotação foi encerrada.</p>
        </div>
    @endif
</div>
