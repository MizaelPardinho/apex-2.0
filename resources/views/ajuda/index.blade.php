@extends('layouts.layout-principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h2>{{__('Dúvidas Frequentes')}}</h2>
                </div>

                <div class="card-body">
                    <div id="accordion">
                        @foreach($faq as $dados)
                            <div class="card mb-2">
                                <a class="card-link text-dark" data-toggle="collapse" href="#faq{{ $loop->index }}">
                                    <div class="card-header">
                                        <b>{{ $dados->duvida }}</b>
                                        <li class="fa fa-level-down"></li>
                                    </div>
                                </a>

                                <div id="faq{{ $loop->index }}" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>
                                            {{ $dados->resposta }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h2>{{__('Chat')}}</h2>
                </div>

                <div class="card-body">
                    <p>{{__('Não encontrou o que procurava?')}}</p>
                    <p>{{__('Tire suas dúvidas no nosso Chat')}}</p>
                    <a href="javascript:void(0);" id="abrir_chat">
                        <p class="text-center">
                            <span class="fa-stack fa-3x">
                                <i class="fa fa-square-o fa-stack-2x"></i>
                                <i class="fa fa-comments-o fa-stack-1x"></i>
                              </span>
                        </p>
                    </a>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">
                    <h2>{{__('Fale Conosco')}}</h2>
                </div>

                <div class="card-body">
                    <p>
                        <a href="faleconosco">{{__('Entre em contato com a gente')}}.</a>
                    </p>

                    <p><a href="tel:556120270202"><i class="fa fa-phone mr-2 ml-4"></i> +55 61 2027-0202</a></p>

                    <p>SAUN, Quadra 5, Lote C, Torre B, 12º a 18º andar</p>

                    <p>Centro Empresarial CNC</p>

                    <p>Asa Norte, Brasília - DF, 70040-250</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
<script>
    $(document).ready(function () {
        $("#abrir_chat").click(function(){

            jivo_api.open();

            // var x = document.getElementById(“jcont”);

            // if (x.style.display == “block”) {
            //     jivo_api.close();
            // }
        });
    });
</script>
@endsection
