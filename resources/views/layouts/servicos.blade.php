<div style="background-color: #F4F4F4; margin-bottom: -110px; margin-top: -91px;">
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-12 text-center my-5 pb-5 pt-3">
                <h1 class="barra_title">{{__('Principais Serviços')}}</h1>
            </div>

            <div class="cidadao col-lg-12">
                <h3 class="servicos-title mb-4">{{__('Nossos Serviços')}}</h3>
            </div>

            <div class="col-lg-3">
                <a href="{{ route('processos-de-aquisicoes' ) }}" class="text-dark no-underline">
                    <div class="servicos-home text-center shadow-sm mb-5">
                        <div class="mt-4"><i class="fa fa-briefcase" style="font-size: 38px; margin-bottom: 18px;"></i></div>
                        <p>{{__('Processos de aquisições')}}</p>

                        <div class="btn-acessar btn btn-primary d-none shadow-sm">
                            <i class="fa fa-link"></i> {{__('Acessar')}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3">
                <a href="{{ route('produtos-servicos' ) }}" class="text-dark no-underline">
                    <div class="servicos-home text-center shadow-sm mb-5">
                        <div class="mt-4"><i class="fa fa-shopping-cart" style="font-size: 38px; margin-bottom: 18px;"></i></div>
                        <p>{{__('Produtos e serviços')}}</p>

                        <div class="btn-acessar btn btn-primary d-none shadow-sm">
                            <i class="fa fa-link"></i> {{__('Acessar')}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3">
                <a href="{{ route('gerenciar-fornecedor' ) }}" class="text-dark no-underline">
                    <div class="servicos-home text-center shadow-sm mb-5">
                        <div class="mt-4"><i class="fa fa-university" style="font-size: 38px; margin-bottom: 18px;"></i></div>
                        <p>{{__('Fornecedores')}}</p>

                        <div class="btn-acessar btn btn-primary d-none shadow-sm">
                            <i class="fa fa-link"></i> {{__('Acessar')}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3">
                <a href="{{ route('cotacoes' ) }}" class="text-dark no-underline">
                    <div class="servicos-home text-center shadow-sm mb-5">
                        <div class="mt-4"><i class="fa fa-bar-chart" style="font-size: 38px; margin-bottom: 18px;"></i></div>
                        <p>{{__('Cotações')}}</p>

                        <div class="btn-acessar btn btn-primary d-none shadow-sm">
                            <i class="fa fa-link"></i> {{__('Acessar')}}
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3">
                <a href="{{ route('legislacao' ) }}" class="text-dark no-underline">
                    <div class="servicos-home text-center shadow-sm mb-5">
                        <div class="mt-4"><i class="fa fa-balance-scale" style="font-size: 38px; margin-bottom: 18px;"></i></div>
                        <p>{{__('Legislação')}}</p>

                        <div class="btn-acessar btn btn-primary d-none shadow-sm">
                            <i class="fa fa-link"></i> {{__('Acessar')}}
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
