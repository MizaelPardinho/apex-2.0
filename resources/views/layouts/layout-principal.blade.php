<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Apex Brasil</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- DataTables -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css" rel="stylesheet">

    <!-- Font-Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
    <div id="app">
        @include('layouts.header')

        <main class="pt-4 pb-4">

            <div class="ajax_load">
                <div class="ajax_load_box">
                    <div class="ajax_load_box_circle"></div>
                    <p class="ajax_load_box_title">Aguarde, carregando...</p>
                </div>
            </div>

            @yield('content')
        </main>

        @yield('footer')

        @include('modals.modal_login')
    </div>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>

    <!-- Masks -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <!-- SELECT2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- Javascript -->
    <script src="{{ asset('js/scripts.js') }}"></script>

    @if(session("localex") == "en")
        <!-- Inglês -->
        <script src="//code.jivosite.com/widget/KPfEMLgrC7" async></script>
    @elseif(session("localex") == "es")
        <!-- Espanhol -->
        <script src="//code.jivosite.com/widget/10hYrjHSB8" async></script>
    @else
        <!-- Português -->
        <script src="//code.jivosite.com/widget/26p39bG8YS" async></script>
    @endif

    <script>
        $(document).ready(function () {

            /*
             * Init DataTable
            */
            var table = $('.dataTable').DataTable({
                "oLanguage": {
                    "sProcessing": "{{__('Processando...')}}",
                    "sLengthMenu": "{{__('_MENU_ resultados por página')}}",
                    "sZeroRecords": "{{__('Não foram encontrados resultados')}}",
                    "sInfo": "{{__('Mostrando de _START_ até _END_ de _TOTAL_ registros')}}",
                    "sInfoEmpty": "{{__('Mostrando de 0 até 0 de 0 registros')}}",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "{{__('Pesquisar')}}:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "{{__('Primeiro')}}",
                        "sPrevious": "{{__('Anterior')}}",
                        "sNext": "{{__('Próximo')}}",
                        "sLast": "{{__('Último')}}"
                    }
                }
            });

            $('.nav-link').on('shown.bs.tab', function() {
                table.columns.adjust().responsive.recalc();
            });

            /*
             * Accordion (Agrupamento Deslizante)
            */
            $('#accordion').on('shown.bs.collapse', function (e) {

                table.columns.adjust().responsive.recalc();

                var panelHeadingHeight = $(this).height();
                var animationSpeed = 900;
                var currentScrollbarPosition = $(document).scrollTop();
                var topOfPanelContent = $(e.target).offset().top - 142;

                if (currentScrollbarPosition > topOfPanelContent - panelHeadingHeight) {
                    $("html, body").animate({ scrollTop: topOfPanelContent}, animationSpeed);
                }
            });
        });

        function recalcularDataTable(id = null) {
            let dataTable = "";

            if (id) {
                dataTable = "#" + id;
            } else {
                dataTable = ".dataTable";
            }

            $(dataTable).DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "{{__('Processando...')}}",
                    "sLengthMenu": "{{__('_MENU_ resultados por página')}}",
                    "sZeroRecords": "{{__('Não foram encontrados resultados')}}",
                    "sInfo": "{{__('Mostrando de _START_ até _END_ de _TOTAL_ registros')}}",
                    "sInfoEmpty": "{{__('Mostrando de 0 até 0 de 0 registros')}}",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "{{__('Pesquisar')}}:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "{{__('Primeiro')}}",
                        "sPrevious": "{{__('Anterior')}}",
                        "sNext": "{{__('Próximo')}}",
                        "sLast": "{{__('Último')}}"
                    }
                },
            }).columns.adjust().responsive.recalc();
        }
    </script>

    @yield('post-script')
</body>
</html>
