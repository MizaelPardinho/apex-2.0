<aside class="dash-sidebar">
    <ul class="dash-sidebar-nav">
        @if (!Auth::user())
            <li class="title text-white text-center"><i class="fa fa-lock"></i> {{mb_strtoupper(__('Entre na sua conta'), 'UTF-8') }}</li>
            <li>
                <a class="nav-link" data-toggle="modal" data-target="#modal_login" href="javascript:;">
                    <i class="fa fa-user"></i> {{mb_strtoupper(__('Faça login'), 'UTF-8') }}
                </a>
            </li>
        @else
            <li class="title text-white text-center">{{ mb_strtoupper(__('Bem-vindo'), 'UTF-8') }}</li>
            <li class="nav-item dropdown">
                <a style="font-size: 14px;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form-dash').submit();">
                        <i class="fa fa-sign-out"></i> {{__('Sair do sistema') }}
                    </a>

                    <form id="logout-form-dash" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endif

        <li><a href="{{ route('processos-de-aquisicoes') }}"><i class="fa fa-search"></i> {{mb_strtoupper(__('Consulta'), 'UTF-8') }}</a></li>

        <li class="title text-white text-center"><i class="fa fa-cogs"></i> {{mb_strtoupper(__('Serviços'), 'UTF-8') }}</li>

        {{-- @if( !Auth::user() || strlen(Auth::user()->cpf) <= 14 ) --}}
        <li><a href="{{ route('processos-de-aquisicoes') }}">{{mb_strtoupper(__('Processos de aquisições'), 'UTF-8') }}</a></li>
        <li><a href="{{ route('produtos-servicos') }}">{{mb_strtoupper(__('Produtos e serviços'), 'UTF-8') }}</a></li>
        <li><a href="{{ route('gerenciar-fornecedor') }}">{{mb_strtoupper(__('Fornecedores'), 'UTF-8') }}</a></li>
        <li><a href="{{ route('cotacoes') }}">{{mb_strtoupper(__('Cotações'), 'UTF-8') }}</a></li>
        <li><a href="{{ route('legislacao') }}">{{mb_strtoupper(__('Legislação'), 'UTF-8') }}</a></li>

        <li class="title text-white text-center">{{mb_strtoupper(__('Faq'), 'UTF-8') }}</li>
        <li><a href="{{ route('faq') }}">{{mb_strtoupper(__('Perguntas e Respostas'), 'UTF-8')}}</a></li>

        <li class="title text-white text-center">{{mb_strtoupper(__('Idioma'), 'UTF-8') }}</li>

        <li>
            <a class="nav-link d-inline" href="{{ route('define-idioma', ['pt-br']) }}">
                <img src="{{ asset('img/apex/pt-br.png') }}" class="Brasil" alt="Brasil" />
            </a>

            <a class="nav-link d-inline" href="{{ route('define-idioma', ['en']) }}">
                <img src="{{ asset('img/apex/en.png') }}" class="Inglês" alt="Inglês" />
            </a>

            <a class="nav-link d-inline" href="{{ route('define-idioma', ['es']) }}">
                <img src="{{ asset('img/apex/es.png') }}" class="Espanhol" alt="Espanhol" />
            </a>
        </li>
        {{-- @endif --}}
    </ul>
</aside>

<nav class="navbar navbar-expand-md navbar-laravel fixed-top py-0 shadow-sm">
    <div class="container-fluid" style="margin-top: 0px !important; padding: 0px !important;">

        <input type="checkbox" id="check" />

        <!-- MENU SIDEBAR MOBILE -->
        <label for="check" class="label-hamburguer" style="padding-right: 20px;">
            <span class="menu-hamburguer mr-2">
                <span class="icon-menu"></span>
                <span class="title-menu">{{mb_strtoupper(__('Serviços'), 'UTF-8')}}</span>
            </span>
        </label>

        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('img/apex/apex_logo.svg') }}" class="logo-desktop" alt="Logo" />
            <img src="{{ asset('img/apex/apex_logo.svg') }}" class="logo-mobile d-none" alt="Logo" />
        </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto menu">

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('processos-de-aquisicoes') }}"><i class="fa fa-search"></i> {{__('Consulta')}}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ajuda',['pagina' => isset($pagina)?$pagina:"Geral"]) }}"><i class="fa fa-question-circle"></i> {{__('Ajuda')}}</a>
                </li>

                <!-- Authentication Links -->
                @if (!Auth::user())
                    <li class="nav-item">
                        <a class="nav-link btn btn-primary btn-login" style="color: #FFF !important;" href="javascript:;" data-toggle="modal" data-target="#modal_login">
                            <i class="fa fa-lock"></i> Login</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> {{__('Sair do sistema')}}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endif
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item mt-1"><i class="fa fa-globe"></i> {{__('Idioma') }}</li>
                <li class="nav-item" style="margin-top: 4px;">
                    <a class="nav-link d-inline" href="{{ route('define-idioma', ['pt-br']) }}">
                        <img src="{{ asset('img/apex/pt-br.png') }}" class="Brasil" alt="Brasil" />
                    </a>

                    <a class="nav-link d-inline" href="{{ route('define-idioma', ['en']) }}">
                        <img src="{{ asset('img/apex/en.png') }}" class="Inglês" alt="Inglês" />
                    </a>

                    <a class="nav-link d-inline mt-1" href="{{ route('define-idioma', ['es']) }}">
                        <img src="{{ asset('img/apex/es.png') }}" class="Espanhol" alt="Espanhol" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
