<footer style="background-color: #FFF;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mt-5">
                <a href="{{ route('faleconosco') }}" style="text-decoration: none">
                    <h3 class="text-white mb-3 ml-4" style="color: #084B7D !important;">
                        <i class="fa fa-envelope"></i> <small>{{__('Fale Conosco - Formulário para Contato com a Coordenação de Aquisições')}}</small>
                    </h3>
                </a>

                <div class="text-white" style="color: #084B7D !important;">
                    <p><a href="tel:556120270202"><i class="fa fa-phone mr-2 ml-4"></i> +55 61 2027-0202</a></p>
                    <p class="mr-2 ml-4">SAUN, Quadra 5, Lote C, Torre B, 12º a 18º andar</p>
                    <p class="mr-2 ml-4">Centro Empresarial CNC</p>
                    <p class="mr-2 ml-4">Asa Norte, Brasília - DF, 70040-250</p>
                </div>

                <div class="social_network">
                    <a href="https://www.facebook.com/apexbrasil" target="_blank"
                       data-placement="bottom" title="Facebook">
                        <span class="fa-stack fa-lg e_tada ml-4">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>

                    <a href="https://twitter.com/apexbrasil" target="_blank"
                       title="Twitter">
                        <span class="fa-stack fa-lg e_tada">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>

                    <a href="https://www.youtube.com/user/ApexBrasil" target="_blank"
                       data-placement="bottom" title="Youtube">
                        <span class="fa-stack fa-lg e_tada">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </div>

                <p class="mt-3 ml-3" style="color: #084B7D !important;">
                    © {{__('Todos os Direitos Reservados')}} - Apex-Brasil
                </p>
            </div>

            <div class="col-lg-6" style="background: #FFF; margin-top: 140px;">
                <img src="{{ asset('img/apex/footer_background.svg') }}" width="530" class="footer-logo-apex-brasil" alt="logo footer" />
            </div>
        </div>
    </div>
</footer>

<style>
    .mensagem-cookie {
        background-color: #004B80;
        color: #FFF;
        width: 100%;
        height: 140px;
        padding: 25px;

        position: fixed;
        bottom: 0;
    }

    .mensagem-cookie .mensagem {
        max-width: 1100px;
        font-size: 16px;

    }
</style>

@auth
    @if (!$checkCookie)
        <div class="row mensagem-cookie">
            <div class="col-lg-10">
                <div class="mensagem">
                    <p>
                        Usamos cookies para permitir que o nosso website funcione corretamente, para personalizar conteúdo e anúncios
                        para proporcionar funcionalidades das redes sociais e para analisar o nosso tráfego.
                    </p>
                </div>
            </div>

            <div class="col-lg-2">
                <button
                    style="background-color: #FFF !important; color: #004B80;"
                    class="btn btn-aceitar-cookies"
                >
                    Aceitar Cookies
                </button>
            </div>
        </div>
    @endif
@endauth

@section('post-script')
<script>
    $(function() {

        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $('.btn-aceitar-cookies').on('click', function() {

            $.ajax({
                url: "{{ route('check-cookie-primeiro-acesso') }}",
                method: "POST",
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);

                    if (response) {
                        $('.mensagem-cookie').hide();
                    }
                },
            });
        });
    });
</script>
@endsection

