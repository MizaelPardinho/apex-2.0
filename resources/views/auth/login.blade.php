@extends('layouts.layout-principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">

                @if (session()->has('usuarioCadastradoComSucesso'))
                    <div class="alert alert-success">
                        {{ session('usuarioCadastradoComSucesso') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-lock"></i> {{__('Faça login na sua conta')}}</h4>
                    </div>

                    <div class="card-body">
                        <form class="" method="POST" action="{{ route('login') }}" id="frm_login" autocomplete="off">
                            @csrf

                            <div class="form-group">
                                <label for="cpf">{{__('Digite seu CPF')}}</label>
                                <input type="text" name="cpf" id="cpf" class="form-control {{ $errors->has('cpf') ? ' is-invalid' : '' }}" value="{{ !empty($cpf) ? $cpf : old('cpf') }}" required="true" {{ empty($cpf) ? 'autofocus' : '' }} />

                                @if ($errors->has('cpf'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('cpf') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password">{{__('Digite sua Senha')}}</label>
                                <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required="true" {{ !empty($cpf) ? 'autofocus' : '' }} />

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! $showRecaptcha !!}

                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" id="btn_submit" class="btn btn-primary"><i class="fa fa-lock"></i> {{__('Acessar')}}</button>
                        </form>

                        <p class="mt-3">
                            <a href="{{ route('password.request', ['cpf' => !empty($cpf) ? $cpf : '' ]) }}">{{__('Esqueceu a sua senha?')}}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="modal-termo" data-backdrop="static">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">{{__('Confirmação')}}</h4>
                    </div>

                    <div class="modal-body">

                        <p>
                            Os dados pessoais solicitados neste formulário são obrigatórios para a identificação do
                            participante nas aquisições da Apex-Brasil. Caso esteja em desacordo com o consentimento a
                            seguir, entende que, por essa razão, não poderá ter acesso às informações e participar
                            dos processos de aquisição da Apex-Brasil. Para fins de exercício
                            de seus direitos (Art. 18, da Lei nº 13.709/2018) ou se tiver alguma dúvida,
                            entre em contato conosco através do e-mail <b>dpo@apexbrasil.com.br</b>.
                        </p>

                        <div class="form-check mb-3">
                            <label class="form-check-label">
                                <input type="checkbox" name="termo" class="form-check-input">
                                {{__('Concordo que os dados pessoais inseridos sejam tratados segundo a LGPD, conforme informado no link abaixo:') }}
                            </label>
                        </div>

                        <a href="http://www.apexbrasil.com.br/emails/relacionamento/2021/TermoDeUso-Portugues.pdf" target="_blank"><i class="fa fa-link"></i> Clique para acessar</a>

                        <br />

                        <button class="btn btn-primary btn-concordo-termo mt-4">{{__('Concordo')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('post-script')

<script>
    var tmpcpf = $("#cpf").val()
    $("#btn_submit").prop('disabled', true);

    function onReCaptchaTimeOut() {
        console.log("Bloquear Botão");
        $("#btn_submit").prop('disabled', true);
    }
    function onReCaptchaSuccess() {
        console.log("Liberar Botão");
        $("#btn_submit").prop('disabled', false);
    }
    function onReCaptchaError() {
        console.log("Erro no captcha");
        $("#btn_submit").prop('disabled', true);
    }

    jQuery(document).ready(function () {
        $('#cpf').mask('000.000.000-00')
    });

    $(function() {

        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $('#frm_login').on('submit', function(e) {
            e.preventDefault();

            const cpf = $('input[name=cpf]').val();

            if (cpf) {

                $.ajax({
                    url: "{{ route('verificar-termo-primeiro-acesso') }}",
                    method: "POST",
                    data: { cpf},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (!response.termoCheck) {
                            $('#modal-termo').modal('show');
                        } else {
                            document.getElementById("frm_login").submit();
                        }
                    },
                });
            }
        });

        $('.btn-concordo-termo').on('click', function() {

            let termo = $('input[name=termo]').is(':checked');

            if (!termo) {
                alert("{{__('Atenção: Para continuar é necessário concordar com o termo.')}}");
                return false;
            }

            const cpf = $('input[name=cpf]').val();

            if (cpf) {
                $.ajax({
                    url: "{{ route('setar-termo-primeiro-acesso') }}",
                    method: "POST",
                    data: { cpf},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            document.getElementById("frm_login").submit();
                        }
                    },
                });
            }
        });
    });
</script>

@endsection
