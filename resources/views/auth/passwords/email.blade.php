@extends('layouts.layout-principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-lock"></i> {{__('Esqueceu a Senha')}}</h4>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('esqueceu-senha') }}">
                            @csrf

                            <div class="form-group">
                                <label for="cpf" class="control-label">CPF:</label>
                                <input id="cpf" type="text" class="form-control col-lg-8 {{ $errors->has('cpf') ? ' is-invalid' : '' }}" name="cpf" value="{{ $_GET['cpf'] ?? old('cpf') }}" required>

                                @if ($errors->has('cpf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $errors->first('cpf') !!}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! $showRecaptcha !!}

                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" id="enviarlink" class="btn btn-primary" disabled><i class="fa fa-lock"></i> {{__('Enviar link para recuperar a senha')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('post-script')
    <script type="text/javascript">

        $("#enviarlink").prop('disabled', true);
        var tmpcpf = $("#cpf").val()

        jQuery(document).ready(function(){
            jQuery('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
            jQuery('.g-recaptcha').attr('data-expired-callback', 'onReCaptchaTimeOut');

            var cpf_opt = {
                onKeyPress: function (cpf, e, field, cpf_opt) {
                    var masks = ['000.000.000-000', '00.000.000/0000-00'];
                    var mask = (cpf.length > 14) ? masks[1] : masks[0];
                    $('#cpf').mask(mask, cpf_opt);
                }
            };

            if (typeof ($("#cpf")) !== "undefined") {
                var masks = ['000.000.000-000', '00.000.000/0000-00'];
                var mask = ($("#cpf").val().length > 14) ? masks[1] : masks[0];
                $("#cpf").mask(mask, cpf_opt);
            }
            $("#cpf").val(tmpcpf) ;
        });

        function onReCaptchaTimeOut(){
            $("#enviarlink").prop('disabled', true);
        }
        function onReCaptchaSuccess(){
            $("#enviarlink").prop('disabled', false);
        }

    </script>
@endsection
