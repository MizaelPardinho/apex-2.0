<div style="padding: 20px;">
    <p>
        <b>Dados da Mensagem:</b> <br />

        Nº Processo: {{ $request->numero_processo ?? 'Não mencionado' }} <br />
        Empresa: {{ $request->empresa ?? 'Não mencionado' }} <br />
        Assunto: {{ $request->assunto }} <br />
        Nome: {{ $request->nome }} <br />
        E-mail: {{ $request->email }} <br />
        Telefone: {{ $request->telefone }}
    </p>

    <p>
        <b>Mensagem:</b> <br />
        {{ $request->mensagem }}
    </p>

</div>
