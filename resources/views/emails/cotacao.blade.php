
<div style="padding: 20px;">
    <h2>Prezado Fornecedor,</h2>

    <p>
        Informamos que sua cotação de preços para o processo nº {{ $numeroProcesso }}
        foi recebida pela APEX na data de {{ date('d/m/Y H:i:s') }}.
    </p>

    <p>Atenciosamente,</p>

    <p>Setor de Aquisições.</p>
</div>
