<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Cotacoes\CotacoesController;
use App\Http\Controllers\FaleConosco\FaleConoscoController;
use App\Http\Controllers\Faq\FaqController;
use App\Http\Controllers\Fornecedores\FornecedoresController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Legislacao\LegislacaoController;
use App\Http\Controllers\Linguagem\IdiomaController;
use App\Http\Controllers\ProcessosDeAquisicoes\ProcessosDeAquisicoesController;
use App\Http\Controllers\ProdutosEservicos\ProdutosEservicosController;
use App\Http\Controllers\Usuarios\UsuarioController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/**
 * Autenticação
 */
Auth::routes();

/**
 * Verificar e setar termo LGPD
 */
Route::post('/verificar-termo-primeiro-acesso', [
    LoginController::class,
    'verificarCheckTermoLGPD'
])->name('verificar-termo-primeiro-acesso');

Route::post('/setar-termo-primeiro-acesso', [
    LoginController::class,
    'setarCheckTermoLGPD'
])->name('setar-termo-primeiro-acesso');

/**
 * Setar cookie primeiro acesso
 */
Route::post('/check-cookie-primeiro-acesso', [
    HomeController::class,
    'setarCheckCookie'
])->name('check-cookie-primeiro-acesso');

/**
 * Validar Usuário
 */
Route::post('/validar-usuario', [
    LoginController::class,
    'validarUsuario'
])->name('validar-usuario');

/**
 * Esqueceu a senha | Resetar senha
 */
Route::post('/esqueceu-senha', [
    UsuarioController::class,
    'forgetPassword'
])->name('esqueceu-senha');

Route::post('/resetar-senha', [
    UsuarioController::class,
    'resetPassword'
])->name('resetar-senha');

/**
 * Home
 */
Route::get('/', [
    HomeController::class,
    'index'
])->name('home');

Route::get('/home', [
    App\Http\Controllers\HomeController::class,
    'index'
])->name('home');

/**
 * Fale Conosco
 */
Route::get('/faleconosco', [
    FaleConoscoController::class,
    'index'
])->name('faleconosco');

Route::post('/enviar-mensagem', [
    FaleConoscoController::class,
    'enviarMensagem'
])->name('enviar-mensagem');

/**
 * Idioma
 */
Route::get('/define-idioma/{locale}', [
    IdiomaController::class,
    'defineIdioma'
])->name('define-idioma');

/**
 * Cadastro usuários
 */
Route::get('/cadastro-usuario', [
    UsuarioController::class,
    'index'
])->name('cadastro-usuario');

Route::post('/cadastro-usuario', [
    UsuarioController::class,
    'store'
])->name('cadastro-usuario');

/**
 * Processos de aquisições
 */
Route::get('/processos-de-aquisicoes', [
    ProcessosDeAquisicoesController::class,
    'index'
])->name('processos-de-aquisicoes');

Route::post('/carregar-documentos-processos-de-aquisicoes', [
    ProcessosDeAquisicoesController::class,
    'carregarDocumentos'
])->name('carregar-documentos-processos-de-aquisicoes');

Route::get('/download-documento-processos-de-aquisicoes/{documento}/{numeroProcesso}', [
    ProcessosDeAquisicoesController::class,
    'downloadDocumento'
])->name('download-documento-processos-de-aquisicoes');

Route::get('/download-comunicado/{documento}', [
    ProcessosDeAquisicoesController::class,
    'downloadComunicado'
])->name('download-comunicado');

Route::post('/notificar-usuario-processos-de-aquisicoes', [
    ProcessosDeAquisicoesController::class,
    'notificarUsuario'
])->name('notificar-usuario-processos-de-aquisicoes');

Route::post('/carregar-comunicados', [
    ProcessosDeAquisicoesController::class,
    'carregarComunicados'
])->name('carregar-comunicados');

Route::post('/carregar-ofertas', [
    ProcessosDeAquisicoesController::class,
    'carregarOfertas'
])->name('carregar-ofertas');

/**
 * Produtos e Serviços
 */
Route::get('/produtos-servicos', [
    ProdutosEservicosController::class,
    'index'
])->name('produtos-servicos');

Route::post('/carregar-produtos-servicos', [
    ProdutosEservicosController::class,
    'carregarProdutosEservicos'
])->name('carregar-produtos-servicos');

/**
 * Fornecedores
 */
Route::get('/gerenciar-fornecedor', [
    FornecedoresController::class,
    'index'
])->name('gerenciar-fornecedor')->middleware('auth');

Route::get('/cadastrar-fornecedor', [
    FornecedoresController::class,
    'create'
])->name('cadastrar-fornecedor')->middleware('auth');

Route::post('/cadastrar-fornecedor', [
    FornecedoresController::class,
    'store'
])->name('cadastrar-fornecedor')->middleware('auth');

Route::get('/editar-fornecedor/{idFornecedor}', [
    FornecedoresController::class,
    'edit'
])->name('editar-fornecedor')->middleware('auth');

Route::post('/editar-fornecedor/{idFornecedor}', [
    FornecedoresController::class,
    'update'
])->name('editar-fornecedor')->middleware('auth');

Route::post('/carregar-informacoes-apartir-cnpj-fornecedor', [
    FornecedoresController::class,
    'carregarInformacoesApartirDoCNPJ'
])->name('carregar-informacoes-apartir-cnpj-fornecedor')->middleware('auth');

Route::post('/excluir-fornecedor/{idFornecedor}', [
    FornecedoresController::class,
    'destroy'
])->name('excluir-fornecedor')->middleware('auth');

Route::post('/salvar-documentos-fornecedores', [
    FornecedoresController::class,
    'enviarDocumento'
])->name('salvar-documentos-fornecedores')->middleware('auth');

Route::post('/remover-documentos-fornecedores', [
    FornecedoresController::class,
    'removerDocumento'
])->name('remover-documentos-fornecedores')->middleware('auth');

/**
 * Cotações
 */
Route::get('/cotacoes', [
    CotacoesController::class,
    'index'
])->name('cotacoes')->middleware('auth');

Route::post('/carregar-cotacao', [
    CotacoesController::class,
    'carregarCotacao'
])->name('carregar-cotacao')->middleware('auth');

Route::post('/salvar-cotacoes-produtos', [
    CotacoesController::class,
    'salvarCotacaoProdutos'
])->name('salvar-cotacoes-produtos')->middleware('auth');

Route::post('/salvar-documentos-cotacoes', [
    CotacoesController::class,
    'enviarDocumento'
])->name('salvar-documentos-cotacoes')->middleware('auth');

Route::post('/remover-documentos-cotacoes', [
    CotacoesController::class,
    'removerDocumento'
])->name('remover-documentos-cotacoes')->middleware('auth');

Route::post('/salvar-documentos-cotacoes-na-base-de-dados', [
    CotacoesController::class,
    'salvarDocumentosNaBaseDeDados'
])->name('salvar-documentos-cotacoes-na-base-de-dados')->middleware('auth');

Route::post('/carregar-documentos-cotacoes', [
    CotacoesController::class,
    'carregarDocumentos'
])->name('carregar-documentos-cotacoes')->middleware('auth');

Route::post('/carregar-notificacoes', [
    CotacoesController::class,
    'carregarNotificacoes'
])->name('carregar-notificacoes')->middleware('auth');

Route::post('/marcar-notificacao-como-lida', [
    CotacoesController::class,
    'marcarNotificacaoComoLida'
])->name('marcar-notificacao-como-lida')->middleware('auth');

Route::post('/salvar-esclarecimento-cotacao', [
    CotacoesController::class,
    'enviarEsclarecimento'
])->name('salvar-esclarecimento-cotacao')->middleware('auth');

/**
 * Legislação
 */
Route::get('/legislacao', [
    LegislacaoController::class,
    'index'
])->name('legislacao');

Route::get('/download-documento-legislacao/{documento}', [
    LegislacaoController::class,
    'downloadDocumento'
])->name('download-documento-legislacao');

/**
 * Faq
 */
Route::get('/faq', [
    FaqController::class,
    'index'
])->name('faq');

Route::get('/ajuda', [
    FaqController::class,
    'ajuda'
])->name('ajuda');
