function enviarDocumento(informacoes) {

    $("body").on("click", ".btn-enviar-documento", function (e) {
        e.preventDefault();

        let load = $(".ajax_load");

        let descricao = $("#descricao_documento").val();
        let arquivo = $("input[name=documento]")[0].files[0];

        if (!descricao) {
            alert("Atenção: Digite a descrição do documento.");
            $("#descricao_documento").trigger('focus');
            return false;
        }

        if (!validarAnexo(arquivo, ["jpg", "jpeg", "png", "pdf"], informacoes.tamanhoAnexoPermitido)) {
            return false;
        }

        const formData = new FormData();
        formData.append("arquivo", arquivo);
        formData.append("descricao", descricao);
        formData.append("servidor", informacoes.servidor);
        formData.append("pastaServidor", informacoes.pastaServidor);
        formData.append("nomeSessao", informacoes.nomeSessao);
        formData.append("caminhoTabela", informacoes.caminhoTabela);

        $.ajax({
            url: informacoes.rota,
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                $("input[name=documento]").val("");
                $("#descricao_documento").val("");
                $("#mensagem_arquivo_cotacao").html("Nenhum arquivo selecionado");

                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentacao").html(response);
                recalcularDataTable(informacoes.idTabela);
            },
        });
    });
}

function removerDocumento(informacoes) {
    $("body").on("click", ".btn-remover-documento", function (e) {
        e.preventDefault();

        let load = $(".ajax_load");

        if (!confirm("Deseja realmente excluir?")) {
            return false;
        }

        let descricao = $(this).data("descricao");
        let caminho = $(this).data("caminho");

        $.ajax({
            url: informacoes.rota,
            method: "POST",
            data: {
                descricao,
                caminho,
                servidor: informacoes.servidor,
                pastaServidor: informacoes.pastaServidor,
                nomeSessao: informacoes.nomeSessao,
                caminhoTabela: informacoes.caminhoTabela
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentacao").html(response);
                recalcularDataTable(informacoes.idTabela);
            },
        });
    });
}
