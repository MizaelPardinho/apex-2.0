/*************** FUNCIONALIDADE ANIMADA DO MENU ***************/

$(function () {
    actionMenu();

    $(".label-hamburguer").click(function () {
        var sidebar = $(".dash-sidebar");

        var session = sessionStorage.getItem("menuOpen");

        if (session == "false" || session == null) {
            sidebar.animate({ left: 0 }, 200, function (e) {
                $("body").css("overflow", "hide");
            });

            sessionStorage.setItem("menuOpen", true);
        } else {
            sidebar.animate({ left: "-320" }, 200, function (e) {
                $("body").css("overflow", "hide");
            });

            sessionStorage.setItem("menuOpen", false);
        }
    });

    function actionMenu() {
        sessionStorage.clear();

        var check = $("#check").is(":checked");
        var sidebar = $(".dash-sidebar");

        if (check) {
            sidebar.animate({ left: 0 }, 200, function (e) {
                $("body").css("overflow", "hide");
            });

            sessionStorage.setItem("menuOpen", true);
        } else {
            sidebar.animate({ left: "-320" }, 200, function (e) {
                $("body").css("overflow", "hide");
            });

            sessionStorage.setItem("menuOpen", false);
        }
    }

    $(".btn-login").click(function () {
        $(".navbar-laravel").css("padding-right", "0px");
        $("body").css("padding-right", "0px");
        $("body").removeClass("modal-open");
    });
});

/*************** FIM FUNCIONALIDADE ANIMADA DO MENU ***************/

/*************** MASK CPF OU CNPJ ***************/

var options = {
    onKeyPress: function (cpf, ev, el, op) {
        var masks = ["000.000.000-000", "00.000.000/0000-00"];
        $(".cpfOuCnpj").mask(cpf.length > 14 ? masks[1] : masks[0], op);
    },
};

$(".cpfOuCnpj").length > 11
    ? $(".cpfOuCnpj").mask("00.000.000/0000-00", options)
    : $(".cpfOuCnpj").mask("000.000.000-00#", options);

/*************** FIM MASK CPF OU CNPJ ***************/

/*************** VALIDA CNPJ ***************/

function validarCNPJ(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g, "");

    if (cnpj == "") return false;

    if (cnpj.length != 14) return false;

    // Elimina CNPJs invalidos conhecidos
    if (
        cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999"
    )
        return false;

    // Valida DVs
    var tamanho = cnpj.length - 2;
    var numeros = cnpj.substring(0, tamanho);
    var digitos = cnpj.substring(tamanho);
    var soma = 0;
    var pos = tamanho - 7;
    for (var i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    var resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado != digitos.charAt(0)) return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (var i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado != digitos.charAt(1)) return false;

    return true;
}

/*************** FIM VALIDA CNPJ ***************/

/*************** VALIDA CPF ***************/

function validarCPF(cpf) {
    var add;
    var rev;
    cpf = cpf.replace(/[^\d]+/g, "");
    if (cpf == "") return false;
    // Elimina CPFs invalidos conhecidos
    if (
        cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999"
    )
        return false;
    // Valida 1o digito
    add = 0;
    for (var i = 0; i < 9; i++) add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11) rev = 0;
    if (rev != parseInt(cpf.charAt(9))) return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i++) add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11) rev = 0;
    if (rev != parseInt(cpf.charAt(10))) return false;
    return true;
}

/*************** FIM VALIDA CPF ***************/

/*************** SCRIPT VIA CEP ***************/

function limpaFormularioCep() {
    document.getElementById("endereco").value = "";
    document.getElementById("bairro").value = "";
    document.getElementById("municipio").value = "";
    document.getElementById("estado").value = "";
}

function meuCallback(conteudo) {
    if (!("erro" in conteudo)) {
        document.getElementById("endereco").value = conteudo.logradouro;
        document.getElementById("bairro").value = conteudo.bairro;
        document.getElementById("municipio").value = conteudo.localidade;
        document.getElementById("estado").value = conteudo.uf;
    } else {
        limpaFormularioCep();
        alert("CEP não encontrado.");
    }
}

function pesquisaCep(cep) {
    var cep = cep.replace(/\D/g, "");

    if (cep != "") {
        var validacep = /^[0-9]{8}$/;

        if (validacep.test(cep)) {
            // $(".ajax_load").fadeIn(200);

            var script = document.createElement("script");

            script.src =
                "https://viacep.com.br/ws/" +
                cep +
                "/json/?callback=meuCallback";

            document.body.appendChild(script);

            // $(".ajax_load").fadeOut(200);
        } else {
            limpaFormularioCep();
            alert("Formato de CEP inválido.");
        }
    } else {
        limpaFormularioCep();
    }
}

/*************** FIM SCRIPT VIA CEP ***************/

/*************** MASCARAS ***************/

$(".mask-tel").mask("(00) 0000-0000");
$(".mask-cel").mask("(00) 00000-0000");
$(".mask-cep").mask("00000-000");
$(".mask-cnpj").mask("00.000.000/0000-00");
$(".mask-rg").mask("00.000.000-0");
$(".mask-cpf").mask("000.000.000-00");
$(".mask-agencia").mask("0000000000");
$(".mask-numero-conta").mask("0000000000");
$(".mask-matricula_imovel").mask("000000");
$(".mask-inscricao_estadual").mask("00000000");
$(".mask-numero_endereco").mask("000000");

function validarAnexo(arquivo, extensoes_suportadas, tamanho = null) {
    if (!arquivo) {
        alert("ATENÇÃO: Escolha um arquivo para enviar.");
        return false;
    }

    let extensao = arquivo.type.split("/")[1];

    if (!extensoes_suportadas.includes(extensao)) {
        alert(
            "Atenção: Só é permitido as seguintes extensões: " +
            extensoes_suportadas.join(", ") +
            "."
        );
        return false;
    }
    if (tamanho == null) {
        if (arquivo.size > 2048000) {
            alert("Atenção: O arquivo deve ter no máximo 2Mb");
            return false;
        }
    } else {
        if (arquivo.size > tamanho) {
            var size = niceBytes(tamanho);
            alert("Atenção: O arquivo deve ter no máximo " + size);
            return false;
        }
    }

    return true;
}

const units = ["bytes", "KB", "MB"];

function niceBytes(x) {
    let l = 0,
        n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }

    return n.toFixed(n < 10 && l > 0 ? 1 : 0) + " " + units[l];
}

function efeito_ancora(elemento) {
    $('html, body').animate({
        scrollTop: $(elemento).offset().top
    }, 2000);
}
